#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <assert.h>

FILE *fin, *fout, *fans, *fsco, *fmsg;
std::ifstream ifs_out;

void score(int s, const char *message) {
	// fprintf(fsco, "%d\n", s);
	// if (fmsg != NULL) fprintf(fmsg, "%s\n", message);
	
	fprintf(fmsg, "%s\n", message);
	fprintf(fmsg, "%d\n", s);
	
	fclose(fmsg);
	
	fclose(fin);
	// fclose(fsco);
	fclose(fans);
	// if (fout != NULL) fclose(fout);
	ifs_out.close();
	// if (fmsg != NULL) fclose(fmsg);
	exit(0);
}

void WA() {
	score(0, "Wrong answer.");
}

void init_SPJ(int argc, char **argv) {
	fin = fopen(argv[1], "r");   // input file 
	fans = fopen(argv[3], "r");
	// fsco = fopen(argv[5], "w");  // score output file (cannot be NULL)
	// fmsg = fopen(argv[6], "w");  // message output file (can be NULL)
	
	#ifdef WYS
		fmsg = stdout;
	#else
		fmsg = fopen("/tmp/_eval.score", "w");
	#endif
	
	fout = fopen(argv[2], "r");  // contestant output file 
	// ifs_out.open(argv[2], std::ifstream::in);
	if (!fin) return exit(1);
	// if (fout == NULL) fileNotFound();
}


const int MAXP = 3005;

int ans[MAXP * 2 + 10], out[MAXP * 2 + 10];



int main(int argc,char **argv) {
	init_SPJ(argc, argv);
	
	int n, k, p;
	fscanf(fin, "%d%d%d", &n, &k, &p);
	
	char ch;
	
	ch = fgetc(fans);
	while (ch >= 48 && ch <= 57) {
		ans[0] = ans[0] * 10 + ch - 48;
		ch = fgetc(fans);
	}
	assert(ch == '.');
	for (int i = 1; i <= 2 * p; i++) {
		ch = fgetc(fans);
		assert(ch >= 48 && ch <= 57);
		ans[i] = ch - 48;
	}
	
	// [0-9]+(.[0-9]+)?((\r)?\n)?
	ch = fgetc(fout);
	while (ch >= 48 && ch <= 57) {
		out[0] = out[0] * 10 + ch - 48;
		if (out[0] > 1000000) {
			WA();
		}
		ch = fgetc(fout);
	}
	if (ch == '.') {
		ch = fgetc(fout);
		if (ch < 48 || ch > 57) {
			WA();
		}
		int cnt = 0;
		while (ch >= 48 && ch <= 57) {
			out[++cnt] = ch - 48;
			ch = fgetc(fout);
			if (cnt > 2 * p) {
				score(0, "Too many digits after the decimal point.");
			}
		}
	}
	if (ch == '\r') {
		ch = fgetc(fout);
		if (ch != '\n') {
			WA();
		} else {
			ch = fgetc(fout);
		}
	} else if (ch == '\n') {
		ch = fgetc(fout);
	}
	if (ch != EOF) {
		WA();
	}
	
	// out - ans
	int N = 2 * p;
	int i;
	for (i = 0; i <= N; i++) {
		if (out[i] != ans[i]) break;
	}
	if (out[i] < ans[i]) {
		for (int i = 0; i <= N; i++) {
			std::swap(out[i], ans[i]);
		}
	}
	
	int last = 0;
	for (int i = N; i >= 1; i--) {
		int tmp = out[i] - ans[i] - last;
		if (tmp < 0) {
			tmp += 10;
			last = 1;
		} else {
			last = 0;
		}
		out[i] = tmp;
	}
	out[0] = out[0] - ans[0] - last;
	
	bool flag_5 = false;
	
	// 1e-5 = 0.00001
	for (int i = 0; i <= 5; i++) {
		if (out[i] != 0) {
			flag_5 = true;
		}
	}
	
	bool flag = false;
	// 1e-p
	for (int i = 0; i <= p; i++) {
		if (out[i] != 0) {
			flag = true;
		}
	}
	
	if (!flag) {
		score(5, "Correct.");
	} else if (!flag_5) {
		score(2, "Partially correct (absolute error < 1e-5).");
	} else {
		WA();
	}
}
