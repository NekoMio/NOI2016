#include <cstdio>
#include <algorithm>
using namespace std;

#define FOR( A, B, C ) for ( int A = B, _end_ = C; A <= _end_; A++ )

double ans = 0, a[ 100 ];

int h[ 100 ], n, m, p, ch[ 10 ][ 10 ];

void Calc() {
	FOR ( i, 1, n ) { a[ i ] = h[ i ]; }
	FOR ( i, 1, m ) {
		double sum = 0; int count = 0;
		FOR ( j, 1, n ) { if ( ch[ i ][ j ] ) sum += a[ j ], count++; }
		if ( count ) sum /= count;
		FOR ( j, 1, n ) { if ( ch[ i ][ j ] ) a[ j ] = sum; }
	}
	if ( a[ 1 ] > ans ) ans = a[ 1 ];
}

#define next( A, B ) B == n ? A + 1 : A, B == n ? 1 : B + 1
void Go( int x, int y ) {
	if ( x == m + 1 ) {
		Calc(); return;
	}
	ch[ x ][ y ] = 0; Go( next( x, y ) );
	ch[ x ][ y ] = 1; Go( next( x, y ) );
}

int main() {
	freopen( "drink.in", "r", stdin );
	freopen( "drink.out", "w", stdout );

	scanf( "%d%d%d", &n, &m, &p );
	FOR ( i, 1, n ) { scanf( "%d", &h[ i ] ); }

	Go( 1, 1 );

	printf( "%lf\n", ans );
}
