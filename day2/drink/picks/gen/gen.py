from random import randint, shuffle;
from os import system;

e9 = int( 1e9 );

#nn = [0, 2, 4, 4, 10, 10, 10, 10, 100, 100, 100, 100, 100, 100, 250, 400, 700, 700, 700, 2500, 5000]
#kk = [0, 0, 0, 5,  1, e9,  0,  0,   1,  e9,  e9,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0]
#pp = [0, 5, 5, 5,  5,  5,  5,  5,   5,   5,  40,  40,  40,  40, 100, 160, 300, 300, 300, 1000, 2000]

nn = [0, 2, 4, 4, 10, 10, 10, 10, 100, 100, 100, 100, 100, 250, 500, 700, 700, 700, 2500, 4000, 8000]
kk = [0, 0, 0, 5,  1, e9,  0,  0,   1,  e9,   0,   0,   0,   0,   0,   0,   0,   0,    0,    0,    0]
pp = [0, 5, 5, 5,  5,  5,  5,  5,   5,  40,  40,  40,  40, 100, 200, 300, 300, 300, 1000, 1500, 3000]

for t in xrange( 20, 21 ) :
    IN = open( "drink.in", "w" )
    n = nn[ t ]; k = kk[ t ]; p = pp[ t ];
    if k == 0 :
        k = randint( 1, n - 1 )
    print >>IN, n, k, p;
    print >>IN, randint( 1, 1000 ), 
    s = set()
    while len( s ) < n - 1 :
        s.add( randint( 1, 100000 ) )
    s = list( s ); shuffle( s )
    for i in xrange( n - 2 ) :
        print >>IN, s[ i ],
    print >>IN, s[ n - 2 ];
    IN.close()
    
    system( "time ./drink" );
    system( "mv drink.in drink" + str( t ) + ".in" )
    system( "mv drink.out drink" + str( t ) + ".out" )
