#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

#define PREC 1.2
#define FOR( A, B, C ) for ( int A = B, _end_ = C; A <= _end_; A++ )
#define ROF( A, B, C ) for ( int A = B, _end_ = C; A >= _end_; A-- )
#define MAX 8000
#define DIGIT 3000
#define ll long long

int n, m, d, p, h[ MAX + 10 ], s[ MAX + 10 ], x[ MAX + 10 ], x0;

int digit = 1000000000;
struct Decimal {
	int inte;
	int deci[ int( PREC * DIGIT / 9 ) ];

	void clear() {
		inte = 0; memset( deci, 0, sizeof( deci ) );
	}

	void operator -= ( const Decimal &A ) {
		inte -= A.inte;
		ROF ( i, p, 1 ) { deci[ i ] -= A.deci[ i ]; if ( deci[ i ] < 0 ) deci[ i ] += digit, deci[ i - 1 ]--; }
		deci[ 0 ] -= A.deci[ 0 ]; if ( deci[ 0 ] < 0 ) deci[ 0 ] += digit, inte--;
	}

	void operator /= ( int d ) {
		ll pre = deci[ 0 ] + ( inte % d ) * (ll)digit; inte /= d;
		FOR ( i, 0, p ) { deci[ i ] = pre / d; pre = deci[ i + 1 ] + ( pre % d ) * digit; }
	}
} f[ 20 ][ MAX + 10 ], ans, y[ MAX + 10 ], k0, k1, y0, st[ MAX + 10 ];

int operator < ( const Decimal &A, const Decimal &B ) {
	if ( A.inte != B.inte ) return A.inte < B.inte;
	FOR ( i, 0, p ) { if ( A.deci[ i ] != B.deci[ i ] ) return A.deci[ i ] < B.deci[ i ]; }
	return 0;
}

int main() {
	freopen( "drink.in", "r", stdin );
	freopen( "drink.out", "w", stdout );

	scanf( "%d%d%d", &n, &m, &d ); p = ( int( PREC * d ) + 8 ) / 9; m = min( n, m );
	FOR ( i, 1, n ) { scanf( "%d", &h[ i ] ); }
	sort( h + 2, h + n + 1 );
	FOR ( i, 1, n ) { s[ i ] = s[ i - 1 ] + h[ i ]; }

	int LIM = 15, larg = min( m, LIM ); // log_3 33*h_i

	FOR ( i, 1, n ) { f[ 0 ][ i ].inte = h[ 1 ]; }
	FOR ( j, 0, larg ) { f[ j ][ 1 ].inte = h[ 1 ]; }

	FOR ( j, 1, larg ) {
		int las = 1, tail = 0;
		x[ ++tail ] = 0; y[ tail ].clear();
		y[ tail ].inte = s[ 1 ]; y[ tail ] -= f[ j - 1 ][ 1 ];
		FOR ( i, 2, n ) {
			st[ las ].clear(); st[ las ].inte = s[ i ]; st[ las ] -= y[ las ]; st[ las ] /= i - x[ las ];
			while ( las < tail ) {
				st[ las + 1 ].clear(); st[ las + 1 ].inte = s[ i ]; st[ las + 1 ] -= y[ las + 1 ]; st[ las + 1 ] /= i - x[ las + 1 ];
				if ( st[ las + 1 ] < st[ las ] ) break;
				las++;
			}
			f[ j ][ i ] = st[ las ];
			if ( f[ j ][ i ] < f[ j - 1 ][ i ] ) f[ j ][ i ] = f[ j - 1 ][ i ];
			y0.clear(); y0.inte = s[ i ]; y0 -= f[ j - 1 ][ i ]; x0 = i - 1;
			while ( tail > 1 ) {
				k0 = y0; k0 -= y[ tail - 1 ]; k0 /= x0 - x[ tail - 1 ];
				k1 = y[ tail ]; k1 -= y[ tail - 1 ]; k1 /= x[ tail ] - x[ tail - 1 ];
				if ( k1 < k0 ) break;
				tail--; if ( las > tail ) las--;
			}
			x[ ++tail ] = x0; y[ tail ] = y0;
		}
	}

	int v = n - max( 0, m - LIM ); ans = f[ larg ][ v ];
	FOR ( i, v + 1, n ) { ans.inte += h[ i ]; ans /= 2; }

	printf( "%d.", ans.inte );
	FOR ( i, 0, p - 1 ) { printf( "%09d", ans.deci[ i ] ); }
}
