#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

#define PREC 1.2
#define FOR( A, B, C ) for ( int A = B, _end_ = C; A <= _end_; A++ )
#define ROF( A, B, C ) for ( int A = B, _end_ = C; A >= _end_; A-- )
#define MAX 1000
#define DIGIT 400
#define ll long long

int n, m, d, p, h[ MAX + 10 ], s[ MAX + 10 ], x[ MAX + 10 ], x0;

int digit = 1000000000;
struct Decimal {
	int inte;
	int deci[ int( PREC * DIGIT / 9 ) ];

	void clear() {
		inte = 0; memset( deci, 0, sizeof( deci ) );
	}

	void operator -= ( const Decimal &A ) {
		inte -= A.inte;
		ROF ( i, p, 1 ) { deci[ i ] -= A.deci[ i ]; if ( deci[ i ] < 0 ) deci[ i ] += digit, deci[ i - 1 ]--; }
		deci[ 0 ] -= A.deci[ 0 ]; if ( deci[ 0 ] < 0 ) deci[ 0 ] += digit, inte--;
	}

	void operator /= ( int d ) {
		ll pre = deci[ 0 ] + ( inte % d ) * (ll)digit; inte /= d;
		FOR ( i, 0, p ) { deci[ i ] = pre / d; pre = deci[ i + 1 ] + ( pre % d ) * digit; }
	}
} f[ MAX + 10 ][ MAX + 10 ], ans, y[ MAX + 10 ], k0, k1, y0;

int operator < ( const Decimal &A, const Decimal &B ) {
	if ( A.inte != B.inte ) return A.inte < B.inte;
	FOR ( i, 0, p ) { if ( A.deci[ i ] != B.deci[ i ] ) return A.deci[ i ] < B.deci[ i ]; }
	return 0;
}

int main() {
	freopen( "drink.in", "r", stdin );
	freopen( "drink.out", "w", stdout );

	scanf( "%d%d%d", &n, &m, &d ); p = ( int( PREC * d ) + 8 ) / 9; m = min( n, m );
	FOR ( i, 1, n ) { scanf( "%d", &h[ i ] ); }
	sort( h + 2, h + n + 1 );
	FOR ( i, 1, n ) { s[ i ] = s[ i - 1 ] + h[ i ]; }

	FOR ( i, 1, n ) { f[ 0 ][ i ].inte = h[ 1 ]; }
	FOR ( j, 0, m ) { f[ j ][ 1 ].inte = h[ 1 ]; }

	FOR ( j, 1, m ) {
		int tail = 0;
		x[ ++tail ] = 0; y[ tail ].clear();
		y[ tail ].inte = s[ 1 ]; y[ tail ] -= f[ j - 1 ][ 1 ];
		FOR ( i, 2, n ) {
			int l = 1, r = tail;
			while ( l < r - 3 ) {
				int sl = ( r - l ) / 3 + l, sr = ( r - l ) * 2 / 3 + l;
				k0.clear(); k0.inte = s[ i ]; k0 -= y[ sl ]; k0 /= i - x[ sl ];
				k1.clear(); k1.inte = s[ i ]; k1 -= y[ sr ]; k1 /= i - x[ sr ];
				if ( k0 < k1 ) l = sl;
				else r = sr;
			}
			f[ j ][ i ] = f[ j - 1 ][ i ];
			FOR ( v, l, r ) {
				k0.clear(); k0.inte = s[ i ]; k0 -= y[ v ]; k0 /= i - x[ v ];
				if ( f[ j ][ i ] < k0 ) f[ j ][ i ] = k0;
			}
			y0.clear(); y0.inte = s[ i ]; y0 -= f[ j - 1 ][ i ]; x0 = i - 1;
			while ( tail > 1 ) {
				k0 = y0; k0 -= y[ tail - 1 ]; k0 /= x0 - x[ tail - 1 ];
				k1 = y[ tail ]; k1 -= y[ tail - 1 ]; k1 /= x[ tail ] - x[ tail - 1 ];
				if ( k1 < k0 ) break;
				tail--;
			}
			x[ ++tail ] = x0; y[ tail ] = y0;
		}
	}

	ans = f[ m ][ n ];
	printf( "%d.", ans.inte );
	FOR ( i, 0, p - 1 ) { printf( "%09d", ans.deci[ i ] ); }
}
