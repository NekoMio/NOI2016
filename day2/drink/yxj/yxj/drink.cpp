#include <ctime>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <algorithm>
const int g_bs = 40;
const int k_max = 15;
using namespace std;

int h[8010];
int temp[2010];

template <int max_size, int base = (int) 1e3, bool d = false>
struct num {
  int n;
  int x[max_size + 1];
  int bs;
  bool d_mode;
  void init(int v) {
    memset(x, 0, sizeof(x));
    n = 0;
    while (v) {
      x[++n] = v % bs;
      v /= bs;
    }
    n = max(n, 1);
  }
  num(int v = 0) {
    bs = base;
    if (d)
      n = max_size - 1;
    else
      n = 1;
    memset(x, 0, sizeof(x));
    d_mode = d;
    if (v != 0)
      init(v);
  }
  void operator += (int v) {
    int m = 0;
    while (v) {
      x[++m] += v % bs;
      v /= bs;
    }
    n = max(n, m);
    for (int i = 1; i <= n; ++i)
      if (x[i] >= bs) {
        x[i] -= bs;
        x[i + 1]++;
      }
    while (x[n] >= bs) {
      x[n] -= bs;
      x[++n]++;
    }
  }
  void operator *= (const num <max_size, base, d> &t) {
    for (int i = 1; i <= n + t.n + 5; ++i)
      temp[i] = 0;
    for (int i = 1; i <= n; ++i)
      for (int j = 1; j <= t.n; ++j)
        temp[i + j - 1] += x[i] * t.x[j];
    n += t.n;
    for (int i = 1; i <= n  ||  temp[i]; ++i) {
      x[i] = temp[i] % bs;
      temp[i + 1] += temp[i] / bs;
      if (i > n)
        n = i;
    }
    while (x[n] == 0 && n > 1)
      --n;
  }
  void operator -= (const num <max_size, base, d> &t) {
    for (int i = 1; i <= n; ++i) {
      x[i] -= t.x[i];
      if (x[i] < 0) {
        x[i] += bs;
        x[i + 1]--;
      }
    }
    while (x[n] == 0  &&  n > 1)
      --n;
  }
  void operator += (const num <max_size, base, d> &t) {
    n = max(n, t.n);
    for (int i = 1; i <= n; ++i)
      x[i] += t.x[i];
    for (int i = 1; i <= n || x[i]; ++i) {
      if (x[i] >= bs) {
        x[i] -= bs;
        x[i + 1]++;
      }
      if (i > n)
        n = i;
    }
  }
  num <max_size, base> operator * (const num <max_size, base, d> &t) {
    num <max_size, base> res = *this;
    res *= t;
    return res;
  }
  num <max_size, base> operator + (const num <max_size, base, d> &t) {
    num <max_size, base> res = *this;
    res += t;
    return res;
  }
  num <max_size, base> operator - (const num <max_size, base, d> &t) {
    num <max_size, base> res = *this;
    res -= t;
    return res;
  }
  bool operator == (const num <max_size, base, d> &t) {
    if (n != t.n)
      return false;
    for (int i = 1; i <= n; ++i)
      if (x[i] != t.x[i])
        return false;
    return true;
  }
  bool operator <= (const num <max_size, base, d> &t) {
    if (n != t.n)
      return n < t.n;
    for (int i = n; i >= 1; --i)
      if (x[i] != t.x[i])
        return x[i] < t.x[i];
    return true;
  }
  bool operator >= (const num <max_size, base, d> &t) {
    if (n != t.n)
      return n > t.n;
    for (int i = t.n; i >= 1; --i)
      if (x[i] != t.x[i])
        return x[i] > t.x[i];
    return true;
  }
};
template <int max_i, int max_d, int bs = (int) 1e8>
struct decimal {
  num <max_i, bs> i;
  num <max_d, bs, true> d;
  void operator += (const int &v) {
    i += v;
  }
  void operator <<= (int t) {
    int r = 0;
    while (t--) {
      for (int j = i.n; j >= 1; --j) {
        i.x[j] = i.x[j] + r * bs;
        r = i.x[j] & 1;
        i.x[j] >>= 1;
      }
      for (int j = d.n; j >= 1; --j) {
        d.x[j] = d.x[j] + r * bs;
        r = d.x[j] & 1;
        d.x[j] >>= 1;
      }
      while (i.x[i.n] == 0  &&  i.n > 1)
        --i.n;
    }
  }
};
template <int max_size>
struct frac {
  num <max_size> m, d;
  frac (int vm = 0, int vd = 1) {
    m.init(vm);
    d.init(vd);
  }
  void init(int vm, int vd) {
    m.init(vm);
    d.init(vd);
  }
  void operator *= (const int &a) {
    num <max_size> v(a);
    m *= v;
  }
  void operator /= (const int &a) {
    num <max_size> v(a);
    d *= v;
  }
  void operator *= (const frac <max_size> &t) {
    m *= t.m;
    d *= t.d;
  }
  void operator -= (const frac <max_size> &t) {
    m *= t.d;
    m -= d * t.m;
    d  = d * t.d;
  }
  void operator += (const frac <max_size> &t) {
    m *= t.d;
    m += d * t.m;
    d = d * t.d;
  }
  frac <max_size> operator * (const frac <max_size> &t) {
    frac <max_size> res = *this;
    res *= t;
    return res;
  }
  frac <max_size> operator + (const frac <max_size> &t) {
    frac <max_size> res = *this;
    res += t;
    return res;
  }
  frac <max_size> operator - (const frac <max_size> &t) {
    frac <max_size> res = *this;
    res -= t;
    return res;
  }
  bool operator == (const frac <max_size> &t) {
    return m * t.d == d * t.m;
  }
  bool operator <= (const frac <max_size> &t) {
    return m * t.d <= d * t.m;
  }
  bool operator >= (const frac <max_size> &t) {
    return m * t.d >= d * t.m;
  }
};
template <int s>
decimal <2, 2000, (int) 1e8> expand(frac <s> t, int p) {
  num <s> m_mid;
  int l = 0, r = (int) 1e5, mid, re;
  const int bs = (int) 1e8;
  for (; mid = (l + r) >> 1, m_mid.init(mid), l <= r; ) {
    if (t.d * m_mid <= t.m) re = (l + r) >> 1, l = mid + 1;
    else r = mid - 1;
  }

  t.m -= t.d * re;
  num <s> ds(bs);
  num <2, bs> re_i(re);
  num <2000, bs, true> re_d(0);

  int pos = 2000;
  for (int i = 1; i <= 2 * p + 10; i += 8) {

    l = 0, r = bs;
    t.m *= ds;
    for (; mid = (l + r) >> 1, m_mid.init(mid), l <= r; )
      if (t.d * m_mid <= t.m) re = mid, l = mid + 1;
      else r = mid - 1;
    re_d.x[--pos] = re;
    t.m -= t.d * re;
  }
  decimal <2, 2000, bs> res;
  res.i = re_i;
  res.d = re_d;
  return res;
}
template <int s>
int cx(pair <int, frac <s> > &a, pair <int, frac <s> > &b, pair <int, frac <s> > &c) {
  frac <s> x = c.second - a.second;
  frac <s> y = b.second - a.second;
  x *= b.first - a.first;
  y *= c.first - a.first;
  if (x == y)
    return 0;
  return x <= y ? -1 : 1;
}

frac <g_bs> s[8010], f[8010], b[8010], temp_f;
pair <int, frac<g_bs> > q[8010], nt, it;

int main() {
  freopen("drink.in", "r", stdin);
  freopen("drink.out", "w", stdout);
  
  time_t T = clock();

  int n, k, m = 1, p, outK, bs = 0;

  cin >> n >> k >> p;
  for (int i = 1; i <= n; ++i)
    cin >> h[i];
  sort(h + 2, h + n + 1);
  for (int i = 2; i <= n; ++i)
    if (h[i] > h[1])
      h[++m] = h[i] - h[1];
  bs = h[1];
  h[1] = 0;
  s[0].init(0, 1);
  n = m;
  for (int i = 1; i <= n; ++i) {
    num <g_bs> ht(h[i]);
    s[i] = s[i - 1];
    s[i].m += ht;
    b[i] = s[i];
  }
  k = min(k, n - 1);
  outK = min(k, k_max);
  for (int i = 1; i <= outK; ++i) {
    int tp = 0, hd = 1;
    for (int j = 2; j <= n - (k - outK); ++j) {
      nt = make_pair(j + 1, s[j]);
      it = make_pair(j - 1, b[j - 1]);
      while (tp >= hd + 1 && cx(q[tp - 1], q[tp], it) < 0)
        tp--;
      q[++tp] = it;
      while (hd < tp && cx(q[hd], q[hd + 1], nt) >= 0)
        hd++;
      f[j] = nt.second - q[hd].second;
      f[j] /= j + 1 - q[hd].first;
    }
    for (int j = 2; j <= n - (k - outK); ++j)  {
      b[j] = s[j] - f[j];
    }
  }
  fprintf(stderr, "%.10lf\n", (double) (clock() - T));
  decimal <2, 2000, (int) 1e8> res = expand(f[n - (k - outK)], p);
  fprintf(stderr, "%.10lf\n", (double) (clock() - T));
  for (int i = n - (k - outK) + 1; i <= n; ++i) {
    res += h[i];
    res <<= 1;
  }
  res += bs;
  printf("%d", res.i.x[res.i.n]);
  for (int i = res.i.n - 1; i >= 1; --i)
    printf("%08d", res.i.x[i]);
  printf(".");
  for (int i = res.d.n; (res.d.n - i + 1) * 8 <= 2 * p; --i)
    printf("%08d", res.d.x[i]);
  fprintf(stderr, "%.10lf\n", (double) (clock() - T));
}
