#include <stdio.h>
#include <string.h>
#include <algorithm>

int n, k, p;

int h[15];
int sum_h[1 << 10];
int cnt[1 << 10];
double f[1 << 10], f1[1 << 10];

int main() {
	freopen("drink.in", "r", stdin);
	freopen("drink.out", "w", stdout);
	scanf("%d%d%d", &n, &k, &p);
	for (int i = 0; i < n; i++) {
		scanf("%d", h + i);
	}
	if (n > 10) return 0;
	if (k > n) k = n;
	for (int i = 1; i < 1 << n; i++) {
		cnt[i] = cnt[i >> 1] + (i & 1);
		for (int j = 0; ; j++) {
			if ((i >> j) & 1) {
				sum_h[i] = sum_h[i ^ (1 << j)] + h[j];
				break;
			}
		}
		f[i] = h[0];
	}
	f[0] = h[0];
	for (int _ = 0; _ < k; _++) {
		for (int i = 0; i < 1 << n; i++) {
			int S = i ^ ((1 << n) - 1);
			for (int j = S; j; j = (j - 1) & S) {
				double tmp = (f[i] + sum_h[j]) / (1 + cnt[j]);
				if (tmp > f1[i | j]) f1[i | j] = tmp;
			}
		}
		memcpy(f, f1, sizeof(f));
	}
	double ans = 0;
	for (int i = 0; i < 1 << n; i++) {
		if (f[i] > ans) ans = f[i];
	}
	if (p == 5) {
		printf("%.10lf\n", ans);
	} else {
		printf("%.20lf\n", ans);
	}
}
