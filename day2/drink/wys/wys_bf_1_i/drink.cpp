#include <stdio.h>
#include <algorithm>
#include <assert.h>
#include <string.h>

struct Int {
	static const int len = 250;
	static const int mo = 1000000000;
	
	int a[len + 1];
	
	Int() {
		memset(a, 0, sizeof(a));
	}
	
	Int(int x) {
		memset(a, 0, sizeof(a));
		a[len] = x;
	}
	
	Int & operator += (int b) {
		a[len] += b;
		return *this;
	}
	
	Int & operator /= (int b) {
		int last = 0;
		for (int i = len; i >= 0; i--) {
			long long tmp = 1LL * last * mo + a[i];
			a[i] = tmp / b;
			last = tmp - 1LL * a[i] * b;
		}
		return *this;
	}
	
	bool operator < (const Int &b) const {
		for (int i = len; i >= 0; i--) {
			if (a[i] < b.a[i]) return true;
			if (a[i] > b.a[i]) return false;
		}
		return false;
	}
	
	void output(int p) {
		assert(p <= len * 9);
		printf("%d.", a[len]);
		int cnt = 0, i;
		for (i = 1; cnt + 9 <= p; i++, cnt += 9) {
			printf("%09d", a[len - i]);
		}
		if (cnt < p) {
			cnt = p - cnt;
			int x = a[len - i];
			char s[100];
			sprintf(s, "%%0%dd", cnt);
			cnt = 9 - cnt;
			while (cnt--) {
				x /= 10;
			}
			printf(s, x);
		}
		printf("\n");
	}
};

const int MAXN = 5005;

int n, k, p;
int h[MAXN];
int s[MAXN];

Int f[MAXN];
Int f_new[MAXN];

void solve() {
	if (n == 0) {
		Int(h[0]).output(p);
		return;
	}
	k = std::min(k, n);
	for (int i = 1; i <= n; i++) {
		s[i] = s[i - 1] + h[i];
		f[i] = h[0];
	}
	f[0] = h[0];
	
	Int ans = h[0];
	for (int c = 1; c <= k; c++) {
		for (int i = c; i <= n; i++) {
			Int t = 0;
			for (int j = c - 1; j < i; j++) {
				// Int tmp = (f[j] + s[i] - s[j]) / (i - j + 1);
				Int tmp = f[j];
				tmp += s[i] - s[j];
				tmp /= i - j + 1;
				if (t < tmp) {
					t = tmp;
				}
			}
			f_new[i] = t;
			ans = std::max(ans, t);
		}
		for (int i = 1; i <= n; i++) {
			f[i] = f_new[i];
		}
	}
	// printf("%.8lf\n", ans);
	ans.output(p);
}

int main() {
	freopen("drink.in", "r", stdin);
	freopen("drink.out", "w", stdout);
	int _n;
	scanf("%d%d%d", &_n, &k, &p);
	p = std::min((int)(1.1 * p) + 10, 2 * p);
	scanf("%d", h + 0);
	n = 0;
	for (int i = 2; i <= _n; i++) {
		int x;
		scanf("%d", &x);
		if (x > h[0]) {
			h[++n] = x;
		}
	}
	std::sort(h + 1, h + n + 1);
	solve();
}
