#include <stdio.h>
#include <algorithm>

const int MAXN = 8005;

int n, k, p;
int h[MAXN];
int s[MAXN];

double f[MAXN];
double f_new[MAXN];

void solve() {
	if (n == 0) {
		printf("%d\n", h[0]);
		return;
	}
	
	k = std::min(k, n);
	for (int i = 1; i <= n; i++) {
		s[i] = s[i - 1] + h[i];
		f[i] = h[0];
	}
	f[0] = h[0];
	
	double ans = h[0];
	
	int K = std::min(14, k);
	
	static int stack[MAXN];
	static double slope[MAXN];
	
	for (int c = 1; c <= K; c++) {
		int L = 1, R = 1;
		stack[1] = c - 1;
		for (int i = c; i <= n; i++) {
			double A = (f[stack[L]] + s[i] - s[stack[L]]) / (i + 1 - stack[L]);
			while (L < R) {
				double B = (f[stack[L + 1]] + s[i] - s[stack[L + 1]]) / (i + 1 - stack[L + 1]);
				if (B > A) {
					A = B;
					++L;
				} else {
					break;
				}
			}
			f_new[i] = A;
			
			// (i, s[i] - f[i])
			A = s[i] - f[i];
			while (L < R) {
				int t = stack[R];
				if (A + f[t] - s[t] < slope[R] * (i - t)) {
					--R;
				} else {
					break;
				}
			}
			int t = stack[R];
			stack[++R] = i;
			slope[R] = (A + f[t] - s[t]) / (i - t);
		}
		for (int i = 0; i <= n; i++) {
			f[i] = f_new[i];
		}
	}
	
	ans = f[n - (k - K)];
	for (int i = n - (k - K) + 1; i <= n; i++) {
		ans = (ans + h[i]) / 2;
	}
	
	printf("%.8lf\n", ans);
}

int main() {
	freopen("drink.in", "r", stdin);
	freopen("drink.out", "w", stdout);
	int _n;
	scanf("%d%d%d", &_n, &k, &p);
	scanf("%d", h + 0);
	n = 0;
	for (int i = 2; i <= _n; i++) {
		int x;
		scanf("%d", &x);
		if (x > h[0]) {
			h[++n] = x;
		}
	}
	std::sort(h + 1, h + n + 1);
	solve();
}
