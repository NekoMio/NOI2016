#include <stdio.h>
#include <algorithm>

const int MAXN = 5005;

int n, k, p;
int h[MAXN];
int s[MAXN];

double f[MAXN];
double f_new[MAXN];

void solve() {
	if (n == 0) {
		printf("%d\n", h[0]);
		return;
	}
	k = std::min(k, n);
	for (int i = 1; i <= n; i++) {
		s[i] = s[i - 1] + h[i];
		f[i] = h[0];
	}
	f[0] = h[0];
	
	double ans = h[0];
	
	int K = std::min(14, k);
	
	for (int c = 1; c <= K; c++) {
		int now = c - 1;
		for (int i = c; i <= n; i++) {
			double A = (f[now] + s[i] - s[now]) / (i - now + 1);
			while (now + 1 < i) {
				double B = (f[now + 1] + s[i] - s[now + 1]) / (i - now);
				if (B > A) {
					A = B;
					++now;
				} else {
					break;
				}
			}
			f_new[i] = A;
		}
		// ans = std::max(ans, f_new[n]);
		for (int i = 1; i <= n; i++) {
			f[i] = f_new[i];
		}
	}
	
	ans = f[n - (k - K)];
	for (int i = n - (k - K) + 1; i <= n; i++) {
		ans = (ans + h[i]) / 2.0;
	}
	
	printf("%.8lf\n", ans);
}

int main() {
	freopen("drink.in", "r", stdin);
	freopen("drink.out", "w", stdout);
	
	int _n;
	scanf("%d%d%d", &_n, &k, &p);
	scanf("%d", h + 0);
	n = 0;
	for (int i = 2; i <= _n; i++) {
		int x;
		scanf("%d", &x);
		if (x > h[0]) {
			h[++n] = x;
		}
	}
	std::sort(h + 1, h + n + 1);
	solve();
}
