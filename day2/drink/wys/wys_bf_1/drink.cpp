#include <stdio.h>
#include <algorithm>

const int MAXN = 5005;

int n, k, p;
int h[MAXN];
int s[MAXN];

double f[MAXN];
double f_new[MAXN];

void solve() {
	if (n == 0) {
		printf("%d\n", h[0]);
		return;
	}
	k = std::min(k, n);
	for (int i = 1; i <= n; i++) {
		s[i] = s[i - 1] + h[i];
		f[i] = h[0];
	}
	f[0] = h[0];
	
	double ans = h[0];
	for (int c = 1; c <= k; c++) {
		for (int i = c; i <= n; i++) {
			double t = 0;
			for (int j = c - 1; j < i; j++) {
				double tmp = (f[j] + s[i] - s[j]) / (i - j + 1);
				if (tmp > t) {
					t = tmp;
				}
			}
			f_new[i] = t;
			ans = std::max(ans, t);
		}
		for (int i = 1; i <= n; i++) {
			f[i] = f_new[i];
		}
	}
	printf("%.8lf\n", ans);
}

int main() {
	freopen("drink.in", "r", stdin);
	freopen("drink.out", "w", stdout);
	int _n;
	scanf("%d%d%d", &_n, &k, &p);
	scanf("%d", h + 0);
	n = 0;
	for (int i = 2; i <= _n; i++) {
		int x;
		scanf("%d", &x);
		if (x > h[0]) {
			h[++n] = x;
		}
	}
	std::sort(h + 1, h + n + 1);
	solve();
}
