#include <stdio.h>
#include <algorithm>

const int MAXN = 500005;

int n, m;

int l[MAXN], r[MAXN], len[MAXN];
int _[MAXN * 2];

bool cmp_len(int i, int j) {
	return len[i] < len[j];
}

struct node {
	node *l, *r;
	int add;
	int ma;
	
	void add_tag(int d) {
		add += d;
		ma += d;
	}
	
	void update() {
		ma = std::max(l->ma, r->ma);
	}
	
	void down() {
		l->add_tag(add);
		r->add_tag(add);
		add = 0;
	}
};

node _nodes[MAXN * 2 * 2], *_tot = _nodes;

node * build(int l, int r) {
	node *ret = ++_tot;
	ret->add = ret->ma = 0;
	if (l < r) {
		int mid = (l + r) >> 1;
		ret->l = build(l, mid);
		ret->r = build(mid + 1, r);
	}
	return ret;
}

void modify(node *a, int l, int r, int d, int L, int R) {
	if (l == L && r == R) {
		a->add_tag(d);
		return;
	}
	a->down();
	int mid = (L + R) >> 1;
	if (r <= mid) {
		modify(a->l, l, r, d, L, mid);
	} else if (l > mid) {
		modify(a->r, l, r, d, mid + 1, R);
	} else {
		modify(a->l, l, mid, d, L, mid);
		modify(a->r, mid + 1, r, d, mid + 1, R);
	}
	a->update();
}

int main() {
	freopen("interval.in", "r", stdin);
	freopen("interval.out", "w", stdout);
	scanf("%d%d", &n, &m);
	
	for (int i = 1; i <= n; i++) {
		scanf("%d%d", l + i, r + i);
		len[i] = r[i] - l[i] + 1;
		_[i] = l[i];
		_[i + n] = r[i];
	}
	
	std::sort(_ + 1, _ + 2 * n + 1);
	
	static int ids[MAXN];
	
	for (int i = 1; i <= n; i++) {
		l[i] = std::lower_bound(_ + 1, _ + 2 * n + 1, l[i]) - _;
		r[i] = std::lower_bound(_ + 1, _ + 2 * n + 1, r[i]) - _;
		ids[i] = i;
	}
	std::sort(ids + 1, ids + n + 1, cmp_len);
	
	int ans = 0x3f3f3f3f;
	
	node *root = build(1, 2 * n);
	int r_id = 1;
	for (int i = 1; i <= n; i++) {
		while (r_id <= n && root->ma < m) {
			int id = ids[r_id];
			modify(root, l[id], r[id], 1, 1, 2 * n);
			++r_id;
		}
		if (root->ma == m) {
			ans = std::min(ans, len[ids[r_id - 1]] - len[ids[i]]);
		}
		modify(root, l[ids[i]], r[ids[i]], -1, 1, 2 * n);
	}
	if (ans == 0x3f3f3f3f) {
		ans = -1;
	}
	printf("%d\n", ans);
}
