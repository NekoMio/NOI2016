#include "testlib.h"
#include <algorithm>
#include <stdio.h>

const int MIN_N = 1;
const int MAX_N = 500000;

const int MIN_X = 0;
const int MAX_X = 1000000000;

int main(int argc, char **argv) {
	
	if (argc - 1 != 1) {
		fprintf(stderr, "usage: %s <test_case_id>\n", argv[0]);
		return 1;
	}
	
	registerValidation();
	
	int n, m;
	
	n = inf.readInt(MIN_N, MAX_N, "n");
	inf.readSpace();
	
	m = inf.readInt(MIN_N, n, "m");
	inf.readEoln();
	
	for (int i = 1; i <= n; i++) {
		int l, r;
		
		l = inf.readInt(MIN_X, MAX_X, "l");
		inf.readSpace();
		
		r = inf.readInt(MIN_X, MAX_X, "r");
		inf.readEoln();
		
		ensuref(l <= r, "l_i must <= r_i");
	}
	
	inf.readEof();
	
	return 0;
}
