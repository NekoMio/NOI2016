#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>

using namespace std;

#define l first
#define r second
#define pb push_back
#define mp make_pair

typedef pair<int, int> pii;
const int N = 1000200;
struct Data {
	int n, m;
	pii a[N];
	void input() {
		scanf("%d%d", &n, &m);
		for (int i = 1; i <= n; i++) {
			scanf("%d%d", &a[i].l, &a[i].r);
		}
	}
};
bool cmpByLen(const pii &t1, const pii &t2) {
	return (t1.r - t1.l) < (t2.r - t2.l);
}
vector<int> slx;
int flag[N * 2];
inline int find(int x) {
	return lower_bound(slx.begin(), slx.end(), x) - slx.begin() + 1;
}
void color(int l, int r, int c, int m, int &fc) {
	int i;
	for (i = l; i <= r; i++) {
		flag[i] += c;
		if (flag[i] == m && c > 0) {
			fc++;
		} 
		if (flag[i] == m - 1 && c < 0) {
			fc--;
		}
	}
}
int solvebf() {
	Data *data = new Data();
	data->input();
	sort(data->a + 1, data->a + 1 + data->n, cmpByLen);
	int n = data->n, m = data->m;
	pii *a = data->a;
	int i, j;
	slx.clear();
	for (i = 1; i <= n; i++) {
		slx.pb(a[i].l);
		slx.pb(a[i].r);
	}
	sort(slx.begin(), slx.end());
	slx.erase(unique(slx.begin(), slx.end()), slx.end());
	int ans = 1 << 30;
	j = 1;
	int fc = 0;
	for (i = 1; i <= n; i++) {
		for (; j <= n && !fc; j++) {
			color(find(a[j].l), find(a[j].r), 1, m, fc);
		}
		if (fc)
			ans = min(ans, (a[j - 1].r - a[j - 1].l) - (a[i].r - a[i].l));
		color(find(a[i].l), find(a[i].r), -1, m, fc);
	}
	if (ans == (1 << 30))
		ans = -1;
	delete data;
	return ans;
}
int main() {
//	makecases();
	freopen("interval.in", "r", stdin);
	freopen("interval.out", "w", stdout);
	printf("%d\n", solvebf());
	fclose(stdin);
	fclose(stdout);
}
