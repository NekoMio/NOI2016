#ifndef __COMMON_H__

#define __COMMON_H__

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

typedef long long lld;

int rand(int a){
	return ((lld(rand()) << 30) ^ (rand() << 15) ^ rand()) % a;
}

template<int n> struct Prime{
	bool isPrime[n + 11];
	int primeCount, prime[n + 11];
	Prime():primeCount(0){
		for(int i = 2; i <= n; i++)
			isPrime[i] = true;
		for(int i = 2; i <= n; i++)
			if(isPrime[i]){
				prime[++primeCount] = i;
				for(int j = (i << 1); j <= n; j += i)
					isPrime[j] = false;
			}
	}
	int operator[](int a)const{
		return prime[a];
	}
	static bool sqrPrime(int a){
		for(int i = 2; i <= a / i; i++)
			if(a % i == 0)
				return false;
		return true;
	}
};

template<int n> struct Number{
	static const int D = 100000000;
	int len, *data;
	Number():len(0){
		data = new int[n + 11];
		data[0] = 0;
	}
	Number(int a){
		data = new int[n + 11];
		data[0] = 0;
		data[1] = a % D;
		data[2] = a / D;
		if(data[2])
			len = 2;
		else if(data[1])
			len = 1;
		else
			len = 0;
	}
	Number(const Number& a){
		data = new int[n + 11];
		len = a.len;
		for(int i = 0; i <= len; i++)
			data[i] = a[i];
	}
	~Number(){
		delete []data;
	}
	Number& operator=(const Number& a){
		len = a.len;
		for(int i = 0; i <= len; i++)
			data[i] = a[i];
	}
	void growLen(int _len){
		for(; len < _len; data[++len] = 0);
	}
	void clearHead(){
		for(; len && !data[len]; len--);
	}
	int& operator[](int a){
		return data[a];
	}
	int operator[](int a)const{
		return data[a];
	}
	void fprint(FILE*ff, const char* st = "")const{
		fprintf(ff, "%d", data[len]);
		for(int i = len - 1; i >= 1; i--)
			fprintf(ff, "%08d", data[i]);
		fprintf(ff, st);
	}
	void print(const char* st = "")const{
		fprint(stdout, st);
	}
	Number& operator+=(const Number& a){
		growLen(a.len);
		int up = 0, cur;
		for(int i = 1; i <= a.len; i++){
			cur = data[i] + a[i] + up;
			data[i] = cur % D;
			up = cur / D;
		}
		for(int i = a.len + 1; up && i <= len; i++){
			cur = data[i] + up;
			data[i] = cur % D;
			up = cur / D;
		}
		if(up)
			data[++len] = up;
		return *this;
	}
	Number& operator-=(const Number& a){
		int up = 0, cur;
		for(int i = 1; i <= a.len; i++){
			cur = data[i] - a[i] + up;
			if(cur < 0){
				data[i] = cur + D;
				up = -1;
			}
			else{
				data[i] = cur;
				up = 0;
			}
		}
		for(int i = a.len + 1; up && i <= len; i++){
			cur = data[i] + up;
			if(cur < 0){
				data[i] = cur + D;
				up = -1;
			}
			else{
				data[i] = cur;
				up = 0;
			}
		}
		clearHead();
		return *this;
	}
	Number& operator*=(int a){
		int up = 0;
		lld cur;
		for(int i = 1; i <= len; i++){
			cur = lld(data[i]) * a + up;
			data[i] = cur % D;
			up = cur / D;
		}
		while(up){
			data[++len] = up % D;
			up /= D;
		}
		return *this;
	}
	void div(int a, int& mo){
		int down = 0;
		lld cur;
		for(int i = len; i >= 1; i--){
			cur = lld(down) * D + data[i];
			data[i] = cur / a;
			down = cur % a;
		}
		clearHead();
		mo = down;
	}
	Number& operator/=(int a){
		int mo;
		div(a, mo);
		return *this;
	}
	Number& operator*=(const Number& a){
		growLen(len + a.len);
		for(int i = len; i >= 1; i--){
			int up = 0, mul = data[i], j = 1;
			data[i] = 0;
			lld cur;
			for(; j <= len; j++){
				cur = data[i + j - 1] + lld(mul) * a[j] + up;
				data[i + j - 1] = cur % D;
				up = cur / D;
			}
			for(; up; j++){
				cur = data[i + j - 1] + up;
				data[i + j - 1] = cur % D;
				up = cur / D;
			}
		}
		clearHead();
		return *this;
	}
	Number operator+(const Number& a)const{
		Number ans = *this;
		ans += a;
		return ans;
	}
	Number operator-(const Number& a)const{
		Number ans = *this;
		ans -= a;
		return ans;
	}
	Number operator*(int a)const{
		Number ans = *this;
		ans *= a;
		return ans;
	}
	Number operator*(const Number& a)const{
		Number ans = *this;
		ans *= a;
		return ans;
	}
	Number operator/(int a)const{
		Number ans = *this;
		ans /= a;
		return ans;
	}
	Number operator/(const Number& a)const{
		Number l = 0, r = *this, mid;
		while(l < r){
			mid = (l + r + 1) / 2;
			if(*this < mid * a)
				r = mid - 1;
			else
				l = mid;
		}
		return l;
	}
	Number operator%(const Number& a)const{
		Number l = 0, r = *this, mid;
		while(l < r){
			mid = (l + r + 1) / 2;
			if(*this < mid * a)
				r = mid - 1;
			else
				l = mid;
		}
		return (*this) - l * a;
	}
	int cmp(const Number& a)const{
		if(len != a.len)
			return (int(len > a.len) << 1) - 1;
		for(int i = len; i >= 1; i--)
			if(data[i] != a[i])
				return (int(data[i] > a[i]) << 1) - 1;
		return 0;
	}
	bool operator==(const Number& a)const{
		return cmp(a) == 0;
	}
	bool operator<(const Number& a)const{
		return cmp(a) == -1;
	}
	bool operator>=(const Number& a)const{
		return cmp(a) != -1;
	}
	lld toInt(){
		lld s = 0;
		for(int i = len; i >= 1; i--)
			s = s * D + data[i];
		return s;
	}
};

template<class T>void gcd(const T& a, const T& b, T&x, T&y, const T& Q){
	if(b == 0){
		x = 1;
		y = 0;
		return;
	}
	T t;
	gcd(b, a % b, t, x, Q);
	T tmp = a / b * x;
	if(tmp < t)
		y = (t - tmp) % Q;
	else{
		y = Q - (tmp - t) % Q;
		if(y == Q)
			y = 0;
	}
}

template<class T>T inv(const T& a, const T& Q){
	T x, y;
	gcd(a, Q, x, y, Q);
	return x;
}

void gcd(int a, int b, int& x, int& y, int Q){
	if(b == 0){
		x = 1;
		y = 0;
		return;
	}
	int t;
	gcd(b, a % b, t, x, Q);
	int tmp = lld(a / b) * x % Q;
	if(tmp < t)
		y = (t - tmp) % Q;
	else{
		y = Q - (tmp - t) % Q;
		if(y == Q)
			y = 0;
	}
}

int inv(int a, int Q){
	int x, y;
	gcd(a, Q, x, y, Q);
	return x;
}

int crt(int m1, int a1, int m2, int a2){
	return (lld(inv(m2, m1) * m2) * a1 + lld(inv(m1, m2) * m1) * a2) % (m1 * m2);
}

template<class T>T crt(const T& m1, const T& a1, const T& m2, const T& a2){
	return (inv(m2, m1) * m2 * a1 + inv(m1, m2) * m1 * a2) % (m1 * m2);
}

#endif
