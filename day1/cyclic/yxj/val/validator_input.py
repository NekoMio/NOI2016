import sys
import re

pattern = '([0-9]+) ([0-9]+) ([0-9]+)[\n]?\Z'
x = re.match(re.compile(pattern), open(sys.argv[1], 'r').read())

if x is None:
    print('failed.')
else:
    n = int(x.group(1))
    m = int(x.group(2))
    k = int(x.group(3))
    if (n in range(1, 10**9+1)) and (m in range(1, 10**9+1)) and (k in range(2, 2001)):
        print('pass.')
    else:
        print('failed.')
