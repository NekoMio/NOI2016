import sys
import re

pattern = '([0-9]+)[\n]?\Z'
x = re.match(re.compile(pattern), open(sys.argv[1], 'r').read())

if x is None:
    print('failed.')
else:
    print('pass.')
