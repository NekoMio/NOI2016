#include <bits/stdc++.h>
#define ll long long
using namespace std;

const int Log = 5, MaxK = 70;
const int MaxN = 50000010, MaxP = 3100000;
int factor[Log], ftot, prod[1 << Log];
ll ans = 0;

char miu[MaxN];
bool notp[MaxN];
int prime[MaxP], ptot;
void init(int n) {
	miu[1] = 1;
	for (int i = 2; i <= n; ++i) {
		if (!notp[i]) {
			prime[++ptot] = i;
			miu[i] = -1;
		}
		for (int j = 1; j <= ptot && prime[j] * i <= n; ++j) {
			notp[prime[j] * i] = 1;
			if (i % prime[j] == 0) break;
			miu[prime[j] * i] = -miu[i];
		}
	}
}
int sum[MaxN];
void init_sum(int n, int d) {
	for (int i = 1; i <= n; ++i) {
		sum[i] = miu[i];
		if (i - d > 0) sum[i] += sum[i - d];
	}
}
int get_sum(int n, int d, int g) {
	int ret = 0;
	for (int i = 0; i < d; ++i)
		if (__gcd(i, d) == g) {
			int tn = n - n % d + i;
			if (tn > n) tn -= d;
			if (tn < 0) continue;
			ret += sum[tn];
		}
//	printf("%d %d %d %d\n", n, d, g, ret);
	return ret;
}
ll calc(int n, int m, int d, int g) {
	if (n == 0 || m == 0) return 0;
	ll ret = 0;
	int lim = min(n, m);
	for (int l = 1, r = 0; l <= lim; l = r + 1) {
		int tn = n / l, tm = m / l;
		r = min(n / tn, m / tm);
		int cnt = get_sum(r, d, g) - get_sum(l - 1, d, g);
		ret += 1ll * cnt * tn * tm;
//		printf("(%d %d) %d %d %d\n", l, r, cnt, tn, tm);
	}
//	printf("%d %d %d %d %lld\n", n, m, d, g, ret);
	return ret;
}

int main() {
	freopen("cyclic.in", "r", stdin);
	freopen("cyclic.out", "w", stdout);
	int n, m, k;
	scanf("%d%d%d", &n, &m, &k);
	swap(n, m);
	init(min(n, m));
	for (int i = 2; i * i <= k; ++i)
		if (k % i == 0) {
			factor[ftot++] = i;
			while (k % i == 0) k /= i;
		}
	if (k != 1) factor[ftot++] = k;
	prod[0] = 1;
	for (int s = 1; s < (1 << ftot); ++s) {
		for (int i = 0; i < ftot; ++i)
			if ((s >> i) & 1) {
				prod[s] = prod[s ^ (1 << i)] * factor[i];
				break;
			}
	}

	for (int s = 0; s < (1 << ftot); ++s) {
		init_sum(min(n, m), prod[s]);
		ll k;
		if (__builtin_popcount(s) & 1)  k = -1;
		else k = 1;
		ans += k * calc(n / prod[s], m, prod[s], 1); 
		for (int t = s; t; t = (t - 1) & s) {
			ll tmp = k * calc(n / prod[s] * prod[t], m, prod[s], prod[t]);
			ans += tmp;
		}
	}
	cout << ans << endl;
	return 0;
}
