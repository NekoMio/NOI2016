#include <bits/stdc++.h>
#define ll long long
using namespace std;

const int Lim = 10000000, MaxN = Lim + 10, MaxP = Lim >> 4, Log = 12;
int n, m, k;

int mp[MaxN], prime[MaxP];
int ptot;
bool notp[MaxN];
void init(int n) {
	for (int i = 2; i <= n; ++i) {
		if (!notp[i]) {
			prime[++ptot] = i;
			mp[i] = i;
		}
		for (int j = 1; j <= ptot && prime[j] * i <= n; ++j) {
			notp[prime[j] * i] = 1;
			mp[prime[j] * i] = prime[j];
			if (i % prime[j] == 0) break;
		}
	}
}

ll ans = 0;
int factor[Log], prod[1 << Log];

int calc(int t) {
	prod[0] = 1;
	int x = t, ftot = 0, ret = 0;
	while (x != 1) {
		int tmp = factor[ftot++] = mp[x];
		while (x % tmp == 0) x /= tmp;
	}
	for (int s = 1; s < (1 << ftot); ++s)
		prod[s] = prod[s - (s & -s)] * factor[31 - __builtin_clz(s & -s)];
	for (int s = 0; s < (1 << ftot); ++s) 
		if (__builtin_popcount(s) & 1) ret -= m / prod[s];
		else ret += m / prod[s];
	return ret;
}

int main() {
	scanf("%d%d%d", &n, &m, &k);
	init(n * k);
	for (int i = 1; i <= n; ++i) ans += calc(i * k / __gcd(i, k));
	cout << ans << endl;
	return 0;
}
