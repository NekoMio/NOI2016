#include <bits/stdc++.h>
#define fir first
#define sec second
#define ll long long
#define pb push_back
using namespace std;

int counter;

const int Log = 5, MaxK = 1 << Log;
const int Lim = 2000, MaxN = Lim + 10;
int factor[Log], ftot, prod[1 << Log];
ll ans = 0;
int k;
map <int, int> fk;

int miu[MaxN];
bool notp[MaxN];
int prime[MaxN], ptot;
int sum[MaxN];
int *dsum[MaxK];
void init_miu(int n) {
	miu[1] = 1;
	for (int i = 2; i <= n; ++i) {
		if (!notp[i]) {
			prime[++ptot] = i;
			miu[i] = -1;
		}
		for (int j = 1; j <= ptot && prime[j] * i <= n; ++j) {
			notp[prime[j] * i] = 1;
			if (i % prime[j] == 0) break;
			miu[prime[j] * i] = -miu[i];
		}
	}
}
void init_sum(int n) {
	for (int i = 1; i <= n; ++i) sum[i] = sum[i - 1] + miu[i];
	for (auto i : fk) {
		dsum[i.sec] = new int[n / i.fir + 1];
		dsum[i.sec][0] = 0;
		for (int j = 1; j * i.fir <= n; ++j) dsum[i.sec][j] = dsum[i.sec][j - 1] + miu[j * i.fir];
	}
}

vector <int> fact[MaxK];
vector <int> get_factor(int x) {
	vector <int> ret;
	for (int i = 1; 1ll * i * i <= x; ++i)
		if (x % i == 0) {
			ret.pb(i);
			if (i * i != x) ret.pb(x / i);
		}
	return ret;
}

class Hash_map {
public:
	static const int mod = 300007, MaxN = 300000;
	int en[mod], next[MaxN], tot;
	ll fir[MaxN], sec[MaxN];

	int count(ll x) {
		for (int i = en[x % mod]; i; i = next[i])
			if (fir[i] == x) return 1;
		return 0;
	}
	ll &operator[] (ll x) {
		int u = x % mod;
		for (int i = en[u]; i; i = next[i])
			if (fir[i] == x) return sec[i];
		next[++tot] = en[u];
		en[u] = tot;
		fir[tot] = x;
		sec[tot] = 0;
		return sec[tot];
	}
}	msum, disc_sum[MaxK];

ll miu_sum(ll n) {
	if (n <= Lim) return sum[n];
	if (msum.count(n)) return msum[n];
	ll &ret = msum[n] = 1;
	for (ll l = 2, r; l <= n; l = r + 1) {
		ll t = n / l;
		r = n / t;
		ret -= (r - l + 1) * miu_sum(t);
	}
	return ret;
}

ll miu_disc_sum(int k, ll n) {
//	for (int i = 1; i <= n; ++i) ret += miu[k * i];
	int pos = fk[k];
	if (n == 0) return 0;
	if (1ll * k * n <= Lim) return dsum[pos][n];
	if (disc_sum[pos].count(n)) return disc_sum[pos][n];
	ll ret = 0;
	ret = miu_sum(n);
	vector <int> &factor = fact[pos];
	for (auto t : factor)
		if (t != 1) ret += miu[t] * miu_disc_sum(t, n / t);
	ret *= miu[k];
	return disc_sum[pos][n] = ret;
}

map <ll, ll> co[MaxK];

void get_sum(ll n, int d, int g, ll t) {
//	for (int i = 1; i <= n; ++i)
//		if (__gcd(i, d) == g) ret += miu[i];
	vector <int> &factor = fact[fk[d / g]];
	for (auto i : factor) {
		assert(miu[i]);
		if (n <= Lim) {
			ans += t * miu[i] * dsum[fk[g * i]][n / (g * i)];
			continue;
		}
		co[fk[g * i]][n / (g * i)] += t * miu[i];
	}
}
void calc(ll n, ll m, int d, int g, int t) {
	if (n == 0 || m == 0) return;
	ll lim = min(n, m);
	for (ll l = 1, r = 0; l <= lim; l = r + 1) {
		ll tn = n / l, tm = m / l;
		r = min(n / tn, m / tm);
		get_sum(r, d, g, 1ll * tn * tm * t);
		get_sum(l - 1, d, g, -1ll * tn * tm * t);
	}
}

int main() {
	ll n, m;
	int total = 0, f[MaxK];
	scanf("%lld%lld%d", &n, &m, &k);
	init_miu(Lim);
	for (int i = 1; i * i <= k; ++i) 
		if (k % i == 0) {
			if (miu[i] != 0) {
				f[total] = i;
				fk[i] = total++;
				fact[fk[i]] = get_factor(i);
			}
			if (i * i == k) break;
			if (miu[k / i] != 0) {
				f[total] = k / i;
				fk[k / i] = total++;
				fact[fk[k / i]] = get_factor(k / i);
			}
		}
	init_sum(Lim);

	for (int i = 2; i * i <= k; ++i)
		if (k % i == 0) {
			factor[ftot++] = i;
			while (k % i == 0) k /= i;
		}
	if (k != 1) factor[ftot++] = k;
	prod[0] = 1;
	for (int s = 1; s < (1 << ftot); ++s) {
		for (int i = 0; i < ftot; ++i)
			if ((s >> i) & 1) {
				prod[s] = prod[s ^ (1 << i)] * factor[i];
				break;
			}
	}

	for (int s = 0; s < (1 << ftot); ++s) {
		ll k;
		if (__builtin_popcount(s) & 1)  k = -1;
		else k = 1;
		calc(m / prod[s], n, prod[s], 1, k); 
		for (int t = s; t; t = (t - 1) & s) 
			calc(m / prod[s] * prod[t], n, prod[s], prod[t], k);
	}

	for (int i = 0; i < total; ++i)
		for (auto j : co[i])
			ans += j.sec * miu_disc_sum(f[i], j.fir);
	cout << ans << endl;

	cerr << counter << endl;
	return 0;
}
