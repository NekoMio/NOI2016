#include "testlib.h"

const int MIN_T = 1;
const int MAX_T = 20;
int testcase;

int T;

const int MAX_LEN[21] = {
	0,
	300,
	300,
	2000,
	2000,
	10,
	10,
	20,
	20,
	30,
	30,
	50,
	50,
	100,
	100,
	200,
	300,
	500,
	1000,
	2000,
	100000,
};

int main(int argc, char **argv) {
	
	if (argc != 2) {
		fprintf(stderr, "usage: %s <test_case_id>\n", argv[0]);
		return 1;
	}
	
	registerValidation();
	
	ensuref(sscanf(argv[1], "%d", &testcase) == 1, "case");
	ensuref(1 <= testcase && testcase <= 20, "case");
	
	T = inf.readInt(MIN_T, MAX_T, "T");
	inf.readEoln();
	
	for (int i = 1; i <= T; i++) {
		std::string a = inf.readLine();
		ensuref(a.length() > 0, "empty");
		ensuref(a.length() <= MAX_LEN[testcase], "too long");
		bool allsame = true;
		for(int j = 1; j < (int) a.length(); j++)
			if(a[j] != a[0]) allsame = false;
		if(testcase <= 4) ensuref(allsame, "not all same");
	}
	
	inf.readEof();
	
	return 0;
}
