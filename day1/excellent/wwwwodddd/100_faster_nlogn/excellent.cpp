#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <set>
using namespace std;
int n, b = 131;
char s[30020];
long long p1 = 1000000007;
long long h1[30020];
long long pw1[30020];
long long p2 = 1000000009;
long long h2[30020];
long long pw2[30020];
//set<pair<int, int> >solutions;
long long calc1(int l, int r) {
	return (h1[r] - h1[l] * pw1[r - l] % p1 + p1) % p1;
}
long long calc2(int l, int r) {
	return (h2[r] - h2[l] * pw2[r - l] % p2 + p2) % p2;
}
int prefix(int x, int y) {
	int L = 0;
	int R = min(n - x, n - y) + 1;
	for (int i = 0; 1 << i < R; i++) {
		if (calc1(x, x + (1 << i)) == calc1(y, y + (1 << i)) && calc2(x, x + (1 << i)) == calc2(y, y + (1 << i))) {
			L = 1 << i;
		} else {
			R = 1 << i;
			break;
		}
	}

	while (L < R - 1) {
		int M = (L + R) / 2;
		if (calc1(x, x + M) == calc1(y, y + M) && calc2(x, x + M) == calc2(y, y + M)) {
			L = M;
		} else {
			R = M;
		}
	}
	return L;
}

int suffix(int x, int y) {
	int L = 0;
	int R = min(x, y) + 1;
	for (int i = 0; 1 << i < R; i++) {
		if (calc1(x - (1 << i), x) == calc1(y - (1 << i), y) && calc2(x - (1 << i), x) == calc2(y - (1 << i), y)) {
			L = 1 << i;
		} else {
			R = 1 << i;
			break;
		}
	}

	while (L < R - 1) {
		int M = (L + R) / 2;
		if (calc1(x - M, x) == calc1(y - M, y) && calc2(x - M, x) == calc2(y - M, y)) {
			L = M;
		} else {
			R = M;
		}
	}
	return L;
}

int L[30020];
int R[30020];
int main() {
	freopen("excellent.in", "r", stdin);
	freopen("excellent.out", "w", stdout);
	int t;
	scanf("%d", &t);
	for (int tt = 0; tt < t; tt++) {
		long long timecounter = 0;
		scanf("%s", s);
		n = strlen(s);
		pw1[0] = 1;
		pw2[0] = 1;
		for (int i = 0; i < n; i++) {
			L[i] = 0;
			R[i] = 0;
			h1[i + 1] = (h1[i] * b + s[i]) % p1;
			h2[i + 1] = (h2[i] * b + s[i]) % p2;
			pw1[i + 1] = (pw1[i] * b) % p1;
			pw2[i + 1] = (pw2[i] * b) % p2;
		}

		for (int i = 1; i <= n; i++) {
			for (int j = i, lcs; j <= n; j += i) {
				lcs = suffix(j - i, j);
				if (lcs >= i) {
					continue;
				}
				int x = j - i - lcs;
				int y = j - lcs;
				int lcp = prefix(x, y);
				if (lcp >= i) {
					L[x] += 1;
					L[y + lcp - 2 * i + 1] -= 1;
					R[x + 2 * i] += 1;
					R[y + lcp + 1] -= 1;
				}
				j += max((lcp - lcs) / i * i, 0);
			}
		}
		for (int i = 0; i < n; i++) {
			L[i + 1] += L[i];
			R[i + 1] += R[i];
			timecounter += (long long)L[i] * R[i];
		}
		printf("%lld\n", timecounter);
	}
	return 0;
}