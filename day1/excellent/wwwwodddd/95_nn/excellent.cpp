#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <set>
using namespace std;
int n, b = 131;
char s[200020];
unsigned long long h[200020];
unsigned long long pw[200020];
//set<pair<int, int> >solutions;
unsigned long long calc(int l, int r) {
	return h[r] - h[l] * pw[r - l];
}
int prefix(int x, int y) {
	int re = 0;
	while (x < n && y < n) {
		if (s[x] == s[y]) {
			re++;
		} else {
			break;
		}
		x++;
		y++;
	}
	return re;
}

int suffix(int x, int y) {
	int re = 0;
	x--;
	y--;
	while (x >= 0 && y >= 0) {
		if (s[x] == s[y]) {
			re++;
		} else {
			break;
		}
		x--;
		y--;
	}
	return re;
}

int L[200020];
int R[200020];
int main() {
	freopen("excellent.in", "r", stdin);
	freopen("excellent.out", "w", stdout);
	int t;
	scanf("%d", &t);
	for (int tt = 0; tt < t; tt++) {
		long long timecounter = 0;
		memset(L, 0, sizeof L);
		memset(R, 0, sizeof R);
		scanf("%s", s);
		n = strlen(s);
		pw[0] = 1;
		for (int i = 0; i < n; i++) {
			h[i + 1] = h[i] * b + s[i];
			pw[i + 1] = pw[i] * b;
		}
//		printf("%d\n", suffix(0, 1));
		for (int i = 1; i <= n; i++) {
			for (int j = i, lcs; j <= n; j += i) {
				lcs = suffix(j - i, j);
				if (lcs >= i) {
					continue;
				}
//				printf("%d %d %d\n", j - i, j, lcs);
				int x = j - i - lcs;
				int y = j - lcs;
				int lcp = prefix(x, y);
//				printf("%d %d %d %d %d\n", i, j, lcp, x, y);
				if (lcp >= i) {
					L[x] += 1;
					L[y + lcp - 2 * i + 1] -= 1;
					R[x + 2 * i] += 1;
					R[y + lcp + 1] -= 1;
//				for (int k = x; k <= y + lcp - 2 * i; k++) {

				}
//				j += lcp / i * i;
			}
		}
		for (int i = 0; i < n; i++) {
			L[i + 1] += L[i];
			R[i + 1] += R[i];
		}
		for (int i = 0; i < n; i++) {
//			if (L[i] && R[i]) {
//				printf("%d %d %d\n", i, L[i], R[i]);
//			}
	//		for (int j = 0; j < L[i].size(); j++) {
	//			for (int k = 0; k < R[i].size(); k++) {
			timecounter += (long long)L[i] * R[i];
	//		if (L[i] == 50000 && R[i] == 50000) {
	//			printf("gunla\n");
	//		}
	//				solutions.insert(make_pair(L[i][j], R[i][j]));
	//			}
	//		}
		}
	//	printf("%lu\n", solutions.size());
		printf("%lld\n", timecounter);
	}

	return 0;
}