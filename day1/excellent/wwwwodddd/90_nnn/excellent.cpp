#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <set>
using namespace std;
int n, b = 131;
char s[200020];
unsigned long long h[200020];
unsigned long long pw[200020];
//set<pair<int, int> >solutions;
unsigned long long calc(int l, int r) {
	return h[r] - h[l] * pw[r - l];
}
int prefix(int x, int y) {
	int re = 0;
	while (x < n && y < n) {
		if (s[x] == s[y]) {
			re++;
		} else {
			break;
		}
		x++;
		y++;
	}
	return re;
}

int suffix(int x, int y) {
	int re = 0;
	x--;
	y--;
	while (x >= 0 && y >= 0) {
		if (s[x] == s[y]) {
			re++;
		} else {
			break;
		}
		x--;
		y--;
	}
	return re;
}

int L[200020];
int R[200020];
int main() {
	freopen("excellent.in", "r", stdin);
	freopen("excellent.out", "w", stdout);
	int t;
	scanf("%d", &t);
	for (int tt = 0; tt < t; tt++) {
		long long timecounter = 0;
		memset(L, 0, sizeof L);
		memset(R, 0, sizeof R);
		scanf("%s", s);
		n = strlen(s);
		for (int i = 0; i < n; i++) {
			// left
//			fprintf(stderr, "%d\n", i);
			long long L = 0;
			long long R = 0;
			for (int j = 0; j < i; j++) {
				if ((i - j) % 2 == 0) {
					bool flag = true;
					for (int k = 0; 2 * k < i - j; k++) {
						if (s[j + k] != s[j + (i - j) / 2 + k]) {
							flag = false;
						}
					}
					if (flag) {
						L++;
					}
				}
			}
			for (int j = i + 1; j <= n; j++) {
				if ((j - i) % 2 == 0) {
					bool flag = true;
					for (int k = 0; 2 * k < j - i; k++) {
						if (s[i + k] != s[i + (j - i) / 2 + k]) {
							flag = false;
						}
					}
					if (flag) {
						R++;
					}
				}
			}
//			if (L && R) {
//				printf("%d %lld %lld\n", i, L, R);
//			}
			timecounter += L * R;
		}
		printf("%lld\n", timecounter);
	}
	return 0;
}