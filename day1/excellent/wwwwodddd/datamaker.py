import sys
import os
import random
n = [300, 300, 2000, 2000, 10, 10, 20, 20, 30, 30, 50, 50, 100, 100, 200, 300, 500, 1000, 2000, 30000]

for tt in range(20):
	sys.stdout = open('excellent%d.in' % (tt + 1), 'w')
	cases = 10
	print cases
	for i in range(cases):
		s = ''
		if tt < 4:
			l = random.randint(n[tt] / 2, n[tt])
			s = chr(random.randint(97, 122)) * l
#		elif tt < 19:
		else:
			l = random.randint(n[tt] / 2, n[tt])
			l = max(l, 10)
			pat = ''
			for j in range(min(i + 1, l / 4)):
				pat += chr(random.randint(97, 122))
			s = pat * (l / len(pat))
			if i >= cases / 2 and l >= 100:
				s = list(s)
				for j in range(int(len(s) ** 0.5)):
					pos = random.randint(0, len(s) - 1)
					let = chr(random.randint(97, 122))
					s[pos] = let
				s = ''.join(s)
		if tt == 19 and i == 9:
			s = open('poor.txt').read().strip()		
		print s
	sys.stdout.close()
	os.system('./a <excellent%d.in >excellent%d.ans' % (tt + 1, tt + 1))
