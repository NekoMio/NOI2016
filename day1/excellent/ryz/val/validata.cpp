#include <cstdio>
#include <cstring>
#include <string>
using namespace std;

const int maxlen[] = {300,300,2000,2000,10,10,20,20,30,30,50,50,100,100,200,300,500,1000,2000,100000};
bool error;

void data_assert(bool b) {
  if (!b) {
    puts("assert error");
    error = 1;
  }
}

int check(int num) {
  printf("test case %d\n",num);
  char buf[10],c;
  char str[1001000];
  int n,m,l,r;
  sprintf(buf,"%d",num);
  string fn = "";
  fn += buf;
  fn += ".in";
  FILE *in = fopen(fn.c_str(),"rb");
  
  fscanf(in,"%d",&n);
  data_assert(1 <= n);
  data_assert(n <= 10);
  for (int i = 0;i < n;i++) {
    int ret = fscanf(in,"%s",str);
    data_assert(ret == 1);
    int len = strlen(str);
    data_assert(len <= maxlen[num - 1]);
    for (int i = 0;i < len;i++) {
      data_assert(str[i] >= 'a' && str[i] <= 'z');
      if (num <= 4 && i > 0)
        data_assert(str[i] == str[i - 1]);
    }
  }
  c = fgetc(in);
  data_assert(c == '\n');
  c = fgetc(in);
  data_assert(c == EOF);
}

int main() {
  for (int i = 1;i <= 20;i++) check(i);
  if (!error) puts("tests pass");
  return 0;
}
