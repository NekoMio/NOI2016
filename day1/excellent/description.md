{% block title %}{% endblock %}

## 【问题描述】

如果一个字符串可以被拆分为 $AABB$ 的形式，其中 $A$ 和 $B$ 是任意**非空**字符串，则我们称该字符串的这种拆分是优秀的。

例如，对于字符串 `aabaabaa`，如果令 $A =$ `aab`，$B =$ `a`，我们就找到了这个字符串拆分成 $AABB$ 的一种方式。

一个字符串可能没有优秀的拆分，也可能存在不止一种优秀的拆分。比如我们令 $A =$ `a`，$B =$ `baa`，也可以用 $AABB$ 表示出上述字符串；但是，字符串 `abaabaa` 就没有优秀的拆分。

现在给出一个长度为 $n$ 的字符串 $S$，我们需要求出，在它**所有子串**的所有拆分方式中，优秀拆分的总个数。这里的子串是指字符串中**连续**的一段。

以下事项需要注意：

1.  出现在不同位置的相同子串，我们认为是不同的子串，它们的优秀拆分均会被计入答案。
2.  在一个拆分中，允许出现 $A = B$。例如 `cccc` 存在拆分 $A = B =$ `c`。
3.  字符串本身也是它的一个子串。

## 【输入格式】

{% block input_file %}{% endblock %}

每个输入文件包含多组数据。输入文件的第一行只有一个整数 $T$，表示数据的组数。保证 $1 \leq T \leq {{ prob.args[0] }}$。

接下来 $T$ 行，每行包含一个仅由英文小写字母构成的字符串 $S$，意义如题所述。

## 【输出格式】

{% block output_file -%}{%- endblock %}

输出 $T$ 行，每行包含一个整数，表示字符串 $S$ 所有子串的所有拆分中，总共有多少个是优秀的拆分。

{% set vars = {} -%}
{%- do vars.__setitem__('sample_id', 1) -%}
{% block sample_text -%}{%- endblock %}

{% block title_sample_description %}{% endblock %}

我们用 $S\left\lbrack i,\ j \right\rbrack$ 表示字符串 $S$ 第 $i$ 个字符到第 $j$ 个字符的子串（从 $1$ 开始计数）。

第一组数据中，共有 $3$ 个子串存在优秀的拆分：

$S\left\lbrack 1,4\right\rbrack =$ `aabb`，优秀的拆分为 $A =$ `a`，$B = \ $`b`；

$S\left\lbrack 3,6 \right\rbrack =$ `bbbb`，优秀的拆分为 $A =$ `b`，$B = \ $`b`；

$S\left\lbrack 1,6\right\rbrack =$ `aabbbb`，优秀的拆分为 $A =$ `a`，$B = \ $`bb`。

而剩下的子串不存在优秀的拆分，所以第一组数据的答案是 $3$。

第二组数据中，有两类，总共 $4$ 个子串存在优秀的拆分：

对于子串 $S\left\lbrack 1,4 \right\rbrack = S\left\lbrack 2,5 \right\rbrack = S\left\lbrack 3,6 \right\rbrack =$ `cccc`，它们优秀的拆分相同，均为 $A =$ `c`，$B = \ $`c`，但由于这些子串位置不同，因此要计算 $3$ 次；

对于子串 $S\left\lbrack 1,6 \right\rbrack =$ `cccccc`，它优秀的拆分有 $2$ 种：$A =$ `c`，$B = \ $`cc` 和 $A =$ `cc`，$B = \ $`c`，它们是相同子串的不同拆分，也都要计入答案。

所以第二组数据的答案是 $3 + 2 = 5$。

第三组数据中，$S\left\lbrack 1,8 \right\rbrack$ 和 $S\left\lbrack 4,11 \right\rbrack$ 各有 $2$ 种优秀的拆分，其中 $S\left\lbrack 1,8 \right\rbrack$ 是问题描述中的例子，所以答案是 $2 + 2 = 4$。

第四组数据中，$S\left\lbrack 1,4 \right\rbrack$，$S\left\lbrack 6,11 \right\rbrack$，$S\left\lbrack 7,12 \right\rbrack$，$S\left\lbrack 2,11 \right\rbrack$，$S\left\lbrack 1,8 \right\rbrack$ 各有 $1$ 种优秀的拆分，$S\left\lbrack 3,14 \right\rbrack$ 有 $2$ 种优秀的拆分，所以答案是 $5 + 2 = 7$。

{% for i in [2, 3] -%}
	{%- do vars.__setitem__('sample_id', i) -%}
	{%- block sample_file -%}{%- endblock %}

{% endfor %}
## 【子任务】

对于全部的测试点，保证 $1 \leq T \leq {{ prob.args[0] }}$。以下对数据的限制均是对于单组输入数据而言的，也就是说同一个测试点下的 $T$ 组数据均满足限制条件。

我们假定 $n$ 为字符串 $S$ 的长度，每个测试点的详细数据范围见下表：

{{ '{{' }} table('data') {{ '}}' }}
