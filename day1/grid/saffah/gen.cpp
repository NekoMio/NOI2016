#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <cstring>
using std::vector;
using std::set;
using std::map;
typedef std::pair<int, int> pii;
#define x first
#define y second
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
#define g(x, y, z) for(int x = (y); x < (z); ++x)
#define h(x, y, z) for(int x = (y); x >= (z); --x)

struct Case{
	int n, m;
	set<pii> points;
	Case(int n_, int m_, const vector<pii> &points_): n(n_), m(m_){
		for(auto &point: points_) if(!addPoint(point)) assert(false);
	}
	Case(int n_, int m_): n(n_), m(m_){
	}
	bool addPoint(const pii &point){
		if(point.x < 1 || point.x > n) return false;
		if(point.y < 1 || point.y > m) return false;
		if(points.count(point)) return false;
		points.insert(point);
		return true;
	}
	void output(FILE *fout){
		fprintf(fout, "%d %d %d\n", n, m, (int) points.size());
		vector<pii> rpoints(points.begin(), points.end());
		std::random_shuffle(rpoints.begin(), rpoints.end());
		for(auto &point: rpoints) fprintf(fout, "%d %d\n", point.x, point.y);
	}
};

struct File{
	int id, t;
	vector<Case> cases;
	File(int id_): id(id_), t(0){
	}
	File(int id_, const vector<Case> &cases_): id(id_), t(cases_.size()), cases(cases_){
	}
	void addCase(const Case &cas){
		cases.push_back(cas); ++t;
	}
	void output(){
		printf("output file %d\n", id);
		char fileName[256];
		sprintf(fileName, "grid%d.in", id);
		FILE *fout = fopen(fileName, "wb");
		fprintf(fout, "%d\n", t);
		std::random_shuffle(cases.begin(), cases.end());
		for(auto &cas: cases) cas.output(fout);
		fclose(fout);
		char cmd[256];
		sprintf(cmd, "std < grid%d.in > grid%d.ans", id, id);
		system(cmd);
	}
};

int R(){
	assert(RAND_MAX == 32767);
	int tmp = rand() * 32768;
	return tmp + rand();
}

namespace randPoints{
	Case gen(int n, int m, int c){
		Case cas(n, m);
		while(c) if(cas.addPoint(pii(R() % n + 1, R() % m + 1))) --c;
		return cas;
	}
}

namespace ansGen{
	bool av[4000086]; int n, m;
	int low[4000086], dep[4000086]; bool hasCut;
	int dx[4] = {-1, 1, 0, 0}, dy[4] = {0, 0, -1, 1};
	void dfsCut(int idx, int fa){
		static int root = -1;
		if(idx == fa) root = idx;
		low[idx] = dep[idx] = dep[fa] + 1;
		int CN = 0, x = idx / m, y = idx % m;
		g(dir, 0, 4){
			int tx = x + dx[dir], ty = y + dy[dir];
			if(tx < 0 || tx >= n || ty < 0 || ty >= m) continue;
			int t = tx * m + ty;
			if(!av[t]) continue;
			if(t == fa) continue;
			if(dep[t]){
				if(dep[t] < low[idx]) low[idx] = dep[t];
			}else{
				dfsCut(t, idx);
				++CN;
				if(low[t] < low[idx]) low[idx] = low[t];
			}
		}
		if(idx == root){
			if(CN > 1) hasCut = true;
		}else g(dir, 0, 4){
			int tx = x + dx[dir], ty = y + dy[dir];
			if(tx < 0 || tx >= n || ty < 0 || ty >= m) continue;
			int t = tx * m + ty;
			if(!av[t]) continue;
			if(dep[t] == dep[idx] + 1 && low[t] >= dep[idx]){
				hasCut = true; break;
			}
		}
	}
	int getAns(){
		memset(low, 0, sizeof(*low) * (n * m));
		memset(dep, 0, sizeof(*dep) * (n * m));
		hasCut = false;
		g(i, 0, n * m) if(av[i]){
			dfsCut(i, i); break;
		}
		g(i, 0, n * m) if(av[i] && !dep[i]) return 0;
		if(hasCut) return 1;
		return 2;
	}
	Case gen2(int n_, int m_, int c, bool nothrow = false){
		int oc = c;
		n = n_; m = m_;
		assert(n * m <= 4000000);
		memset(av, 1, sizeof(*av) * (n * m));
		printf("gen2 %d %d %d begin\n", n, m, c);
		int cnt = c / 10;
		int T = 100;
		if(nothrow) T = 1000000;
		while(T--){
			if(!nothrow) printf("%d remaining\n", c);
			if(c <= 0) break;
			while(cnt > c) --cnt;
			vector<int> points;
			g(i, 0, cnt){
				begin:;
				int idx = R() % (n * m);
				if(!av[idx]) goto begin;
				points.push_back(idx);
			}
			for(auto idx: points) av[idx] = false;
			if(getAns() == 2) c -= cnt;
			else{
				for(auto idx: points) av[idx] = true;
				if(cnt > 1) --cnt;
			}
		}
		if(T <= 0){
			if(nothrow) return gen2(n_, m_, oc, nothrow); else throw 233;
		}
		printf("gen2 %d %d %d finished\n", n, m, c);
		Case cas(n, m);
		g(i, 0, n * m) if(!av[i]) cas.addPoint(pii(i / m + 1, i % m + 1));
		return cas;
	}
	Case gen1(int n_, int m_, int c, bool nothrow = false){
		int oc = c;
		n = n_; m = m_;
		assert(n * m <= 4000000);
		memset(av, 1, sizeof(*av) * (n * m));
		printf("gen1 %d %d %d begin\n", n, m, c);
		int cnt = c / 10;
		int T = 100;
		if(nothrow) T = 1000000;
		while(T--){
			if(!nothrow) printf("%d remaining\n", c);
			if(c <= 1) break;
			while(cnt + 1 > c) --cnt;
			vector<int> points;
			g(i, 0, cnt){
				begin:;
				int idx = R() % (n * m);
				if(!av[idx]) goto begin;
				points.push_back(idx);
			}
			for(auto idx: points) av[idx] = false;
			if(getAns() == 2) c -= cnt;
			else{
				for(auto idx: points) av[idx] = true;
				if(cnt > 1) --cnt;
			}
		}
		if(T <= 0){
			if(nothrow) return gen1(n_, m_, oc, nothrow); else throw 233;
		}
		if(nothrow) T = 1000000;
		T = 100;
		while(T--){
			vector<int> points;
			g(i, 0, 1){
				begin2:;
				int idx = R() % (n * m);
				if(!av[idx]) goto begin2;
				points.push_back(idx);
			}
			for(auto idx: points) av[idx] = false;
			if(getAns() == 1) break;
			else for(auto idx: points) av[idx] = true;
		}
		if(T <= 0){
			if(nothrow) return gen1(n_, m_, oc, nothrow); else throw 233;
		}
		printf("gen1 %d %d %d finished\n", n, m, c);
		Case cas(n, m);
		g(i, 0, n * m) if(!av[i]) cas.addPoint(pii(i / m + 1, i % m + 1));
		return cas;
	}
	Case gen0(int n_, int m_, int c, bool nothrow = false){
		int oc = c;
		n = n_; m = m_;
		assert(n * m <= 4000000);
		memset(av, 1, sizeof(*av) * (n * m));
		printf("gen0 %d %d %d begin\n", n, m, c);
		int cnt = c / 10;
		int T = 100;
		if(nothrow) T = 1000000;
		while(T--){
			if(!nothrow) printf("%d remaining\n", c);
			if(c <= 1) break;
			while(cnt + 1 > c) --cnt;
			vector<int> points;
			g(i, 0, cnt){
				begin:;
				int idx = R() % (n * m);
				if(!av[idx]) goto begin;
				points.push_back(idx);
			}
			for(auto idx: points) av[idx] = false;
			if(getAns() >= 1) c -= cnt;
			else{
				for(auto idx: points) av[idx] = true;
				if(cnt > 1) --cnt;
			}
		}
		if(T <= 0){
			if(nothrow) return gen0(n_, m_, oc, nothrow); else throw 233;
		}
		if(nothrow) T = 1000000;
		T = 100;
		while(T--){
			vector<int> points;
			g(i, 0, 1){
				begin2:;
				int idx = R() % (n * m);
				if(!av[idx]) goto begin2;
				points.push_back(idx);
			}
			for(auto idx: points) av[idx] = false;
			if(getAns() == 0) break;
			else for(auto idx: points) av[idx] = true;
		}
		if(T <= 0){
			if(nothrow) return gen0(n_, m_, oc, nothrow); else throw 233;
		}
		printf("gen0 %d %d %d finished\n", n, m, c);
		Case cas(n, m);
		g(i, 0, n * m) if(!av[i]) cas.addPoint(pii(i / m + 1, i % m + 1));
		return cas;
	}
}

namespace sparse{
	Case gen(int n, int m, int c, int cnt = 128){
		// printf("gen %d %d %d\n", n, m, c);
		Case cas(n, m);
		if(c / m > n) c = n * m;
		while(c--){
			begin:;
			set<pii> points;
			points.insert(pii(1, 2));
			points.insert(pii(1, m - 1));
			points.insert(pii(2, 1));
			points.insert(pii(2, m));
			points.insert(pii(n - 1, 1));
			points.insert(pii(n - 1, m));
			points.insert(pii(n, 2));
			points.insert(pii(n, m - 1));
			for(auto &point: cas.points) f(dx, -1, 1) f(dy, -1, 1) if(dx || dy){
				int tx = point.x + dx, ty = point.y + dy;
				if(tx < 1 || tx > n || ty < 1 || ty > n) continue;
				points.insert(pii(tx, ty));
			}
			vector<pii> vp(points.begin(), points.end());
			vp.push_back(pii(-1, 1));
			vp.push_back(pii(-1, 2));
			vp.push_back(pii(-1, 3));
			vp.push_back(pii(-1, 4));
			f(i, 1, cnt) vp.push_back(pii(-1, 5));
			pii cp = vp[R() % (int) vp.size()];
			if(cp.x == -1){
				if(cp.y == 1) cp = pii(1, R() % m + 1);
				else if(cp.y == 2) cp = pii(n, R() % m + 1);
				else if(cp.y == 3) cp = pii(R() % n + 1, 1);
				else if(cp.y == 4) cp = pii(R() % n + 1, m);
				else if(cp.y == 5) cp = pii(R() % n + 1, R() % m + 1);
			}
			if(!cas.addPoint(cp)) goto begin;
		}
		return cas;
	}
}

namespace expand{
	map<int, int> disc(const set<int> &S, int dn){
		vector<pii> V;
		for(auto x: S) V.push_back(pii(x, 0));
		int vn = (int) V.size();
		vector<int *> vp;
		g(i, 1, vn){
			V[i].y = V[i].x - V[i - 1].x;
			if(V[i].y > 2) vp.push_back(&V[i].y);
		}
		if(vp.empty()) g(i, 1, vn) if(V[i].y > 1) vp.push_back(&V[i].y);
		if(vp.empty()) vp.push_back(&V[R() % (vn - 1) + 1].y);
		while(dn){
			int cnt = dn / 100 + 1;
			*vp[R() % (int) vp.size()] += cnt;
			dn -= cnt;
		}
		map<int, int> M;
		g(i, 1, vn){
			V[i].y += V[i - 1].y;
			M[V[i].x] = V[i].y;
		}
		return M;
	}
	Case gen(int n, int m, const Case &oriCase, int c = -1){
		if(c == -1) c = oriCase.points.size();
		printf("expand %d %d from %d %d\n", n, m, oriCase.n, oriCase.m);
		set<int> sx, sy;
		sx.insert(0); sx.insert(oriCase.n + 1);
		sy.insert(0); sy.insert(oriCase.m + 1);
		for(auto &point: oriCase.points){
			sx.insert(point.x); sy.insert(point.y);
		}
		map<int, int> mx = disc(sx, n - oriCase.n), my = disc(sy, m - oriCase.m);
		Case cas(n, m);
		for(auto &point: oriCase.points) cas.addPoint(pii(mx[point.x], my[point.y]));
		while((int) cas.points.size() < c) cas.addPoint(pii(R() % n + 1, R() % m + 1));
		return cas;
	}
}



int main(){
	/*srand(121449137);
	File(1, {
		Case(
			1, 1, {}
		),
		Case(
			1, 1, {pii(1, 1)}
		),
		Case(
			1, 2, {}
		),
		Case(
			2, 1, {pii(1, 1)}
		),
		Case(
			2, 1, {pii(1, 1), pii(2, 1)}
		),
		Case(
			3, 1, {}
		),
		Case(
			1, 3, {pii(1, 2)}
		),
		Case(
			1, 3, {pii(1, 1), pii(1, 3)}
		),
		Case(
			3, 1, {pii(2, 1), pii(3, 1)}
		),
		Case(
			1, 3, {pii(1, 1), pii(1, 2), pii(1, 3)}
		),
		Case(
			4, 1, {}
		),
		Case(
			4, 1, {pii(1, 1)}
		),
		Case(
			1, 4, {pii(1, 1), pii(1, 2)}
		),
		Case(
			1, 4, {pii(1, 1), pii(1, 4)}
		),
		Case(
			1, 4, {pii(1, 2), pii(1, 4)}
		),
		Case(
			2, 2, {}
		),
		Case(
			2, 2, {pii(1, 2)}
		),
		Case(
			2, 2, {pii(1, 1), pii(1, 2)}
		),
		Case(
			2, 2, {pii(1, 1), pii(2, 2)}
		),
		Case(
			2, 2, {pii(1, 1), pii(1, 2), pii(2, 1), pii(2, 2)}
		)
	}).output();
	srand(121449137);
	File(2, {
		randPoints::gen(1, 8, 0),
		randPoints::gen(1, 7, 1),
		randPoints::gen(8, 1, 8),
		randPoints::gen(7, 1, 7),
		randPoints::gen(7, 1, 0),
		randPoints::gen(2, 4, 8),
		randPoints::gen(2, 4, 3),
		randPoints::gen(2, 4, 6),
		randPoints::gen(4, 2, 1),
		randPoints::gen(4, 2, 4),
		randPoints::gen(4, 2, 7),
		randPoints::gen(2, 3, 0),
		randPoints::gen(2, 3, 1),
		randPoints::gen(2, 3, 2),
		randPoints::gen(3, 2, 3),
		randPoints::gen(3, 2, 4),
		randPoints::gen(3, 2, 5),
		Case(
			1, 7, {pii(1, 1), pii(1, 2), pii(1, 3), pii(1, 4), pii(1, 5), pii(1, 6)}
		),
		Case(
			1, 8, {pii(1, 1), pii(1, 2), pii(1, 3), pii(1, 4), pii(1, 5), pii(1, 6)}
		),
		Case(
			8, 1, {pii(1, 1)}
		)
	}).output();
	srand(121449137);
	File(3, {
		randPoints::gen(1, 3, 0),
		randPoints::gen(5, 3, 0),
		randPoints::gen(3, 5, 1),
		randPoints::gen(15, 1, 1),
		randPoints::gen(5, 3, 2),
		randPoints::gen(1, 14, 2),
		randPoints::gen(7, 2, 3),
		randPoints::gen(2, 7, 4),
		randPoints::gen(3, 5, 5),
		randPoints::gen(5, 3, 6),
		randPoints::gen(3, 4, 7),
		randPoints::gen(4, 3, 8),
		randPoints::gen(5, 3, 9),
		randPoints::gen(3, 5, 10),
		randPoints::gen(5, 3, 11),
		randPoints::gen(3, 5, 12),
		randPoints::gen(7, 2, 13),
		randPoints::gen(5, 3, 14),
		randPoints::gen(3, 5, 15),
		randPoints::gen(15, 1, 15)
	}).output();
	srand(121449137);
	File(4, {
		randPoints::gen(1, 28, 0),
		randPoints::gen(2, 15, 2),
		randPoints::gen(3, 10, 1),
		randPoints::gen(3, 10, 3),
		randPoints::gen(4, 7, 8),
		randPoints::gen(4, 7, 10),
		randPoints::gen(5, 6, 2),
		randPoints::gen(5, 6, 4),
		randPoints::gen(5, 6, 6),
		randPoints::gen(5, 5, 8),
		randPoints::gen(6, 5, 15),
		randPoints::gen(6, 5, 20),
		randPoints::gen(6, 5, 27),
		randPoints::gen(7, 4, 5),
		randPoints::gen(7, 4, 3),
		randPoints::gen(9, 3, 2),
		randPoints::gen(9, 3, 0),
		randPoints::gen(15, 2, 0),
		randPoints::gen(30, 1, 1)
	}).output();
	srand(121449137);
	File(5, {
		randPoints::gen(10, 10, 0),
		randPoints::gen(10, 10, 3),
		randPoints::gen(10, 10, 6),
		randPoints::gen(10, 10, 10),
		randPoints::gen(10, 10, 15),
		randPoints::gen(10, 10, 20),
		randPoints::gen(10, 10, 35),
		randPoints::gen(10, 10, 50),
		randPoints::gen(10, 10, 75),
		randPoints::gen(10, 10, 100),
		randPoints::gen(9, 11, 5),
		randPoints::gen(9, 11, 10),
		randPoints::gen(9, 11, 20),
		randPoints::gen(9, 11, 40),
		randPoints::gen(9, 11, 80),
		randPoints::gen(11, 9, 5),
		randPoints::gen(11, 9, 10),
		randPoints::gen(11, 9, 20),
		randPoints::gen(11, 9, 40),
		randPoints::gen(11, 9, 80)
	}).output();
	srand(121449137);
	File(6, {
		randPoints::gen(17, 17, 0),
		randPoints::gen(17, 17, 1),
		randPoints::gen(17, 17, 2),
		randPoints::gen(17, 17, 4),
		randPoints::gen(17, 17, 8),
		randPoints::gen(17, 17, 16),
		randPoints::gen(17, 17, 32),
		randPoints::gen(17, 17, 64),
		randPoints::gen(17, 17, 128),
		randPoints::gen(17, 17, 256),
		randPoints::gen(15, 20, 10),
		randPoints::gen(15, 20, 20),
		randPoints::gen(15, 20, 40),
		randPoints::gen(15, 20, 80),
		randPoints::gen(15, 20, 160),
		randPoints::gen(20, 15, 10),
		randPoints::gen(20, 15, 20),
		randPoints::gen(20, 15, 40),
		randPoints::gen(20, 15, 80),
		randPoints::gen(20, 15, 160)
	}).output();
	srand(121449137);
	File(7, {
		randPoints::gen(31, 32, 0),
		randPoints::gen(31, 32, 2),
		randPoints::gen(31, 32, 4),
		randPoints::gen(31, 32, 8),
		randPoints::gen(31, 32, 16),
		randPoints::gen(31, 32, 32),
		randPoints::gen(31, 32, 64),
		randPoints::gen(31, 32, 128),
		randPoints::gen(31, 32, 256),
		randPoints::gen(31, 32, 512),
		randPoints::gen(32, 31, 2),
		randPoints::gen(32, 31, 4),
		randPoints::gen(32, 31, 8),
		randPoints::gen(32, 31, 16),
		randPoints::gen(32, 31, 32),
		randPoints::gen(32, 31, 64),
		randPoints::gen(32, 31, 128),
		randPoints::gen(32, 31, 256),
		randPoints::gen(32, 31, 512),
		randPoints::gen(32, 31, 992)
	}).output();
	srand(121449137);
	File(8, {
		sparse::gen(1, 20000, 0),
		sparse::gen(20000, 1, 1),
		sparse::gen(2, 10000, 2),
		sparse::gen(10000, 2, 3),
		sparse::gen(5, 4000, 3),
		sparse::gen(4000, 5, 4),
		sparse::gen(10, 2000, 4),
		sparse::gen(2000, 10, 4),
		sparse::gen(20, 1000, 5),
		sparse::gen(1000, 20, 5),
		sparse::gen(50, 400, 5),
		sparse::gen(50, 400, 5),
		sparse::gen(400, 50, 5),
		sparse::gen(400, 50, 5),
		sparse::gen(100, 200, 5),
		sparse::gen(100, 200, 5),
		sparse::gen(100, 200, 5),
		sparse::gen(200, 100, 5),
		sparse::gen(200, 100, 5),
		sparse::gen(200, 100, 5)
	}).output();
	srand(121449137);
	File(9, {
		sparse::gen(1, 20000, 0),
		sparse::gen(20000, 1, 1),
		sparse::gen(2, 10000, 2),
		sparse::gen(10000, 2, 3),
		sparse::gen(5, 4000, 4),
		sparse::gen(4000, 5, 5),
		sparse::gen(10, 2000, 6),
		sparse::gen(2000, 10, 7),
		sparse::gen(20, 1000, 8),
		sparse::gen(1000, 20, 9),
		sparse::gen(50, 400, 10),
		sparse::gen(50, 400, 11),
		sparse::gen(400, 50, 12),
		sparse::gen(400, 50, 13),
		sparse::gen(100, 200, 14),
		sparse::gen(100, 200, 15),
		sparse::gen(100, 200, 15),
		sparse::gen(200, 100, 15),
		sparse::gen(200, 100, 15),
		sparse::gen(200, 100, 15)
	}).output();
	srand(121449137);
	File(10, {
		sparse::gen(1, 20000, 0),
		sparse::gen(20000, 1, 1),
		sparse::gen(2, 10000, 2),
		sparse::gen(10000, 2, 3),
		sparse::gen(5, 4000, 4),
		sparse::gen(4000, 5, 6),
		sparse::gen(10, 2000, 8),
		sparse::gen(2000, 10, 10),
		sparse::gen(20, 1000, 12),
		sparse::gen(1000, 20, 14),
		sparse::gen(50, 400, 16),
		sparse::gen(50, 400, 18),
		sparse::gen(400, 50, 20),
		sparse::gen(400, 50, 22),
		sparse::gen(100, 200, 24),
		sparse::gen(100, 200, 26),
		sparse::gen(100, 200, 27),
		sparse::gen(200, 100, 28),
		sparse::gen(200, 100, 29),
		sparse::gen(200, 100, 30)
	}).output();
	srand(121449137);
	File(11, {
		ansGen::gen2(141, 141, 5000, true),
		ansGen::gen1(141, 141, 5000, true),
		ansGen::gen0(141, 141, 5000, true),
		randPoints::gen(141, 141, 290),
		randPoints::gen(141, 141, 290),
		randPoints::gen(141, 141, 290),
		randPoints::gen(141, 141, 290),
		randPoints::gen(141, 141, 290),
		randPoints::gen(141, 141, 290),
		randPoints::gen(141, 141, 290),
		randPoints::gen(100, 200, 290),
		randPoints::gen(100, 200, 290),
		randPoints::gen(100, 200, 290),
		randPoints::gen(100, 200, 290),
		randPoints::gen(100, 200, 290),
		randPoints::gen(200, 100, 290),
		randPoints::gen(200, 100, 290),
		randPoints::gen(200, 100, 290),
		randPoints::gen(200, 100, 290),
		randPoints::gen(200, 100, 290)
	}).output();
	srand(121449137);
	File(12, {
		ansGen::gen2(316, 316, 5000, true),
		ansGen::gen1(316, 316, 5000, true),
		ansGen::gen0(316, 316, 5000, true),
		randPoints::gen(316, 316, 290),
		randPoints::gen(316, 316, 290),
		randPoints::gen(316, 316, 290),
		randPoints::gen(316, 316, 290),
		randPoints::gen(316, 316, 290),
		randPoints::gen(316, 316, 290),
		randPoints::gen(316, 316, 290),
		randPoints::gen(250, 400, 290),
		randPoints::gen(250, 400, 290),
		randPoints::gen(250, 400, 290),
		randPoints::gen(250, 400, 290),
		randPoints::gen(250, 400, 290),
		randPoints::gen(400, 250, 290),
		randPoints::gen(400, 250, 290),
		randPoints::gen(400, 250, 290),
		randPoints::gen(400, 250, 290),
		randPoints::gen(400, 250, 290)
	}).output();
	srand(121449137);
	File(13, {
		ansGen::gen2(547, 548, 5000, true),
		ansGen::gen1(547, 548, 5000, true),
		expand::gen(547, 548, ansGen::gen0(316, 316, 5000, true)),
		randPoints::gen(547, 548, 290),
		randPoints::gen(547, 548, 290),
		randPoints::gen(547, 548, 290),
		randPoints::gen(547, 548, 290),
		randPoints::gen(547, 548, 290),
		randPoints::gen(547, 548, 290),
		randPoints::gen(547, 548, 290),
		randPoints::gen(500, 600, 290),
		randPoints::gen(500, 600, 290),
		randPoints::gen(500, 600, 290),
		randPoints::gen(500, 600, 290),
		randPoints::gen(500, 600, 290),
		randPoints::gen(600, 500, 290),
		randPoints::gen(600, 500, 290),
		randPoints::gen(600, 500, 290),
		randPoints::gen(600, 500, 290),
		randPoints::gen(600, 500, 290)
	}).output();*/
	srand(121449137);
	File(14, {
		expand::gen(1000, 1000, ansGen::gen2(547, 548, 5000, true)),
		expand::gen(1000, 1000, ansGen::gen1(547, 548, 5000, true)),
		expand::gen(1000, 1000, ansGen::gen0(316, 316, 5000, true)),
		randPoints::gen(1000, 1000, 290),
		randPoints::gen(1000, 1000, 290),
		randPoints::gen(1000, 1000, 290),
		randPoints::gen(1000, 1000, 290),
		randPoints::gen(1000, 1000, 290),
		randPoints::gen(1000, 1000, 290),
		randPoints::gen(1000, 1000, 290),
		randPoints::gen(400, 2500, 290),
		randPoints::gen(400, 2500, 290),
		randPoints::gen(400, 2500, 290),
		randPoints::gen(400, 2500, 290),
		randPoints::gen(400, 2500, 290),
		randPoints::gen(2500, 400, 290),
		randPoints::gen(2500, 400, 290),
		randPoints::gen(2500, 400, 290),
		randPoints::gen(2500, 400, 290),
		randPoints::gen(2500, 400, 290)
	}).output();
	/*srand(121449137 + 1);
	{
		const int sz[20] = {300, 600, 1000, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 80, 160, 320, 640, 1280, 2560};
		// const int bl[20] = {6000, 6000, 6000, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
		File file(15);
		g(i, 0, 20){
			begin15:;
			try{
				if(i >= 3) file.addCase(expand::gen(20000, 20000, randPoints::gen(sz[i], sz[i], 100)));
				else if(i == 0) file.addCase(expand::gen(20000, 20000, ansGen::gen0(sz[i], sz[i], 6000)));
				else if(i == 1) file.addCase(expand::gen(20000, 20000, ansGen::gen1(sz[i], sz[i], 4000), 6000));
				else if(i == 2) file.addCase(expand::gen(20000, 20000, ansGen::gen2(sz[i], sz[i], 2000), 6000));
			}catch(int){
				goto begin15;
			}
		}
		file.output();
	}
	srand(121449137 + 1);
	{
		const int sz[20] = {700, 1300, 1300, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 150, 200, 400, 800, 1600, 3200};
		// const int bl[20] = {6000, 6000, 6000, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
		File file(16);
		g(i, 0, 20){
			begin16:;
			try{
				if(i >= 3) file.addCase(expand::gen(100000, 100000, randPoints::gen(sz[i], sz[i], 500)));
				else if(i == 0) file.addCase(expand::gen(100000, 100000, ansGen::gen0(sz[i], sz[i], 30000)));
				else if(i == 1) file.addCase(expand::gen(100000, 100000, ansGen::gen1(sz[i], sz[i], 20000), 30000));
				else if(i == 2) file.addCase(expand::gen(100000, 100000, ansGen::gen2(sz[i], sz[i], 20000), 30000));
			}catch(int){
				goto begin16;
			}
		}
		file.output();
	}
	srand(121449137);
	File(17, {
		Case(
			1, 1, {}
		),
		Case(
			1, 2, {}
		),
		Case(
			1, 3, {}
		),
		Case(
			1, 999999999, {}
		),
		Case(
			1, 1000000000, {}
		),
		Case(
			2, 1, {}
		),
		Case(
			2, 2, {}
		),
		Case(
			2, 3, {}
		),
		Case(
			2, 999999999, {}
		),
		Case(
			2, 1000000000, {}
		),
		Case(
			999999999, 1, {}
		),
		Case(
			999999999, 2, {}
		),
		Case(
			999999999, 3, {}
		),
		Case(
			999999999, 999999999, {}
		),
		Case(
			999999999, 1000000000, {}
		),
		Case(
			1000000000, 1, {}
		),
		Case(
			1000000000, 2, {}
		),
		Case(
			1000000000, 3, {}
		),
		Case(
			1000000000, 999999999, {}
		),
		Case(
			1000000000, 1000000000, {}
		)
	}).output();
	srand(121449137);
	File(18, {
		Case(
			1, 1, {pii(1, 1)}
		),
		Case(
			1, 1, {}
		),
		Case(
			2, 1, {pii(1, 1)}
		),
		Case(
			3, 1, {pii(3, 1)}
		),
		Case(
			1, 5, {pii(1, 1)}
		),
		Case(
			2, 2, {pii(1, 1)}
		),
		Case(
			2, 3, {pii(2, 2)}
		),
		Case(
			5, 5, {pii(3, 3)}
		),
		Case(
			3, 3, {pii(2, 2)}
		),
		Case(
			999999999, 1000000000, {pii(999999999, 1000000000)}
		),
		Case(
			999999999, 1000000000, {}
		),
		Case(
			999999999, 1000000000, {pii(1, 2)}
		),
		Case(
			999999999, 1000000000, {pii(1, 1)}
		),
		Case(
			999999999, 1000000000, {pii(233, 2333333)}
		),
		Case(
			1000000000, 1, {}
		),
		Case(
			1000000000, 1, {pii(2333333, 1)}
		),
		Case(
			1, 1000000000, {pii(1, 1000000000)}
		),
		Case(
			2, 1000000000, {pii(1, 1000000000)}
		),
		Case(
			2, 1000000000, {pii(1, 1)}
		),
		Case(
			3, 1000000000, {pii(1, 1)}
		)
	}).output();
	srand(121449137);
	File(19, {
		Case(
			1, 1, {pii(1, 1)}
		),
		Case(
			1, 2, {pii(1, 1), pii(1, 2)}
		),
		Case(
			1, 3, {pii(1, 2)}
		),
		Case(
			1, 4, {pii(1, 1), pii(1, 4)}
		),
		Case(
			1, 5, {pii(1, 1), pii(1, 2)}
		),
		Case(
			2, 2, {pii(1, 2), pii(2, 1)}
		),
		Case(
			2, 3, {pii(2, 2), pii(2, 3)}
		),
		Case(
			2, 1000000000, {pii(1, 23333333), pii(2, 23333334)}
		),
		Case(
			3, 3, {pii(2, 2), pii(3, 3)}
		),
		Case(
			999999999, 1000000000, {pii(999999999, 1000000000), pii(999999999, 999999999)}
		),
		Case(
			999999999, 1000000000, {pii(999999998, 1000000000), pii(999999999, 999999999)}
		),
		Case(
			999999999, 1000000000, {pii(1, 2), pii(1, 3)}
		),
		Case(
			999999999, 1000000000, {pii(1, 1), pii(1, 999999999)}
		),
		Case(
			999999999, 1000000000, {pii(233, 2333333), pii(1, 999999999)}
		),
		Case(
			1000000000, 1, {pii(1, 1), pii(7, 1)}
		),
		Case(
			1000000000, 1, {pii(2333333, 1), pii(2333334, 1)}
		),
		Case(
			4, 1000000000, {pii(2, 33333333), pii(3, 33333333)}
		),
		Case(
			3, 1000000000, {pii(2, 33333333), pii(3, 33333333)}
		),
		Case(
			2, 1000000000, {pii(1, 1), pii(2, 1)}
		),
		Case(
			3, 1000000000, {pii(1, 1), pii(2, 2)}
		)
	}).output();
	srand(121449137);
	File(20, {
		sparse::gen(1, 1, 1, 4),
		sparse::gen(1, 2, 1, 4),
		sparse::gen(1, 999999999, 3, 4),
		sparse::gen(1, 1000000000, 3, 4),
		sparse::gen(2, 1, 1, 4),
		sparse::gen(2, 2, 1, 4),
		sparse::gen(2, 999999999, 3, 4),
		sparse::gen(2, 1000000000, 3, 4),
		sparse::gen(999999999, 1, 1, 4),
		sparse::gen(999999999, 2, 1, 4),
		sparse::gen(999999999, 999999999, 3, 4),
		sparse::gen(999999999, 1000000000, 3, 4),
		sparse::gen(1000000000, 1, 1, 4),
		sparse::gen(1000000000, 2, 1, 4),
		sparse::gen(1000000000, 999999999, 3, 4),
		sparse::gen(1000000000, 1000000000, 3, 4),
		Case(
			1000000000, 1000000000, {pii(6, 6), pii(7, 7), pii(8, 6)}
		),
		Case(
			1000000000, 1000000000, {pii(1, 6), pii(2, 7), pii(1, 8)}
		),
		Case(
			1000000000, 1000000000, {pii(1, 6), pii(2, 7), pii(23333, 33333)}
		),
		Case(
			1000000000, 1000000000, {pii(1, 6), pii(2, 7), pii(1, 7)}
		)
	}).output();
	srand(121449137);
	File(21, {
		sparse::gen(1, 1, 1),
		sparse::gen(1, 2, 1),
		sparse::gen(1, 3, 2),
		sparse::gen(1, 999999999, 10),
		sparse::gen(1, 1000000000, 10),
		sparse::gen(2, 1, 1),
		sparse::gen(2, 2, 1),
		sparse::gen(2, 3, 2),
		sparse::gen(2, 999999999, 10),
		sparse::gen(2, 1000000000, 10),
		sparse::gen(999999999, 1, 10),
		sparse::gen(999999999, 2, 10),
		sparse::gen(999999999, 3, 10),
		sparse::gen(999999999, 999999999, 10),
		sparse::gen(999999999, 1000000000, 10),
		sparse::gen(1000000000, 1, 10),
		sparse::gen(1000000000, 2, 10),
		sparse::gen(1000000000, 3, 10),
		sparse::gen(1000000000, 999999999, 10),
		sparse::gen(1000000000, 1000000000, 10),
	}).output();
	srand(121449137);
	File(22, {
		sparse::gen(1, 1, 1, 512),
		sparse::gen(1, 2, 1, 512),
		sparse::gen(1, 3, 2, 512),
		sparse::gen(1, 999999999, 30, 512),
		sparse::gen(1, 1000000000, 30, 512),
		sparse::gen(2, 1, 1, 512),
		sparse::gen(2, 2, 1, 512),
		sparse::gen(2, 3, 2, 512),
		sparse::gen(2, 999999999, 30, 512),
		sparse::gen(2, 1000000000, 30, 512),
		sparse::gen(999999999, 1, 30, 512),
		sparse::gen(999999999, 2, 30, 512),
		sparse::gen(999999999, 3, 30, 512),
		sparse::gen(999999999, 999999999, 30, 512),
		sparse::gen(999999999, 1000000000, 30, 512),
		sparse::gen(1000000000, 1, 30, 512),
		sparse::gen(1000000000, 2, 30, 512),
		sparse::gen(1000000000, 3, 30, 512),
		sparse::gen(1000000000, 999999999, 30, 512),
		sparse::gen(1000000000, 1000000000, 30, 512),
	}).output();
	srand(121449137);
	{
		const int sz[20] = {50, 50, 50, 50, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 200, 400, 800, 1600, 3200};
		File file(23);
		g(i, 0, 20){
			begin:;
			try{
				file.addCase(expand::gen(1000000000, 1000000000, randPoints::gen(sz[i], sz[i], 300)));
			}catch(int){
				goto begin;
			}
		}
		file.output();
	}
	srand(121449137);
	{
		const int sz[20] = {300, 600, 1000, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 80, 160, 320, 640, 1280, 2560};
		// const int bl[20] = {6000, 6000, 6000, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
		File file(24);
		g(i, 0, 20){
			begin24:;
			try{
				if(i >= 3) file.addCase(expand::gen(1000000000, 1000000000, randPoints::gen(sz[i], sz[i], 100)));
				else if(i == 0) file.addCase(expand::gen(1000000000, 1000000000, ansGen::gen0(sz[i], sz[i], 6000)));
				else if(i == 1) file.addCase(expand::gen(1000000000, 1000000000, ansGen::gen1(sz[i], sz[i], 4000), 6000));
				else if(i == 2) file.addCase(expand::gen(1000000000, 1000000000, ansGen::gen2(sz[i], sz[i], 2000), 6000));
			}catch(int){
				goto begin24;
			}
		}
		file.output();
	}
	srand(121449137);
	{
		const int sz[20] = {700, 1300, 1300, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 150, 200, 400, 800, 1600, 3200};
		// const int bl[20] = {6000, 6000, 6000, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
		File file(25);
		g(i, 0, 20){
			begin25:;
			try{
				if(i >= 3) file.addCase(expand::gen(1000000000, 1000000000, randPoints::gen(sz[i], sz[i], 500)));
				else if(i == 0) file.addCase(expand::gen(1000000000, 1000000000, ansGen::gen0(sz[i], sz[i], 30000)));
				else if(i == 1) file.addCase(expand::gen(1000000000, 1000000000, ansGen::gen1(sz[i], sz[i], 20000), 30000));
				else if(i == 2) file.addCase(expand::gen(1000000000, 1000000000, ansGen::gen2(sz[i], sz[i], 20000), 30000));
			}catch(int){
				goto begin25;
			}
		}
		file.output();
	}
	srand(121449137 + 233);
	File(0, {
		randPoints::gen(10, 10, 5),
		randPoints::gen(10, 10, 10),
		randPoints::gen(10, 10, 20),
		randPoints::gen(10, 10, 40),
		randPoints::gen(10, 10, 80),
		randPoints::gen(100, 100, 8),
		randPoints::gen(100, 100, 40),
		randPoints::gen(100, 100, 200),
		randPoints::gen(100, 100, 1000),
		randPoints::gen(100, 100, 5000),
		randPoints::gen(1000, 1000, 8),
		randPoints::gen(1000, 1000, 80),
		randPoints::gen(1000, 1000, 800),
		randPoints::gen(1000, 1000, 8000),
		randPoints::gen(1000, 1000, 80000),
		sparse::gen(1000000000, 1000000000, 0, 0),
		sparse::gen(1000000000, 1000000000, 1, 0),
		sparse::gen(1000000000, 1000000000, 2, 0),
		sparse::gen(1000000000, 1000000000, 3, 0),
		sparse::gen(1000000000, 1000000000, 4, 0)
	}).output();*/
	return 0;
}
