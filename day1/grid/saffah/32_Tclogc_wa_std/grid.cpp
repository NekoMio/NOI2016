#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
#define g(x, y, z) for(int x = (y); x < (z); ++x)
#define MAXN 100007
#define MAXC 100007
typedef long long LL;
// 顺着边界走，区域在左边
struct Point{
	int x, y;
};
inline bool operator <(const Point &a, const Point &b){
	if(a.x != b.x) return a.x < b.x; else return a.y < b.y;
}
inline bool operator ==(const Point &a, const Point &b){
	return a.x == b.x && a.y == b.y;
}
inline bool operator !=(const Point &a, const Point &b){
	return a.x != b.x || a.y != b.y;
}
inline Point operator +(const Point &a, const Point &b){
	return (Point) {a.x + b.x, a.y + b.y};
}
inline Point operator -(const Point &a, const Point &b){
	return (Point) {a.x - b.x, a.y - b.y};
}
inline Point leftTurn(const Point &a){
	return (Point) {-a.y, a.x};
}
inline Point rightTurn(const Point &a){
	return (Point) {a.y, -a.x};
}
Point D[4] = {(Point) {1, 0}, (Point) {0, 1}, (Point) {-1, 0}, (Point) {0, -1}};
Point S[4] = {(Point) {0, 0}, (Point) {-1, 0}, (Point) {-1, -1}, (Point) {0, -1}};
Point E[4] = {(Point) {0, -1}, (Point) {0, 0}, (Point) {-1, 0}, (Point) {-1, -1}};

int n, m;
inline bool valid(const Point &a){
	return a.x >= 0 && a.x <= n + 1 && a.y >= 0 && a.y <= m + 1;
}
Point ob[MAXN * 4 + MAXC]; int obn;
Point xi[MAXN * 4 + MAXC * 4]; int xin;
Point si[MAXN * 4 + MAXC * 4], ei[MAXN * 4 + MAXC * 4]; int sen;
int head[MAXN * 4 + MAXC * 8], from[MAXN * 4 + MAXC * 4], til[MAXN * 4 + MAXC * 20], next[MAXN * 4 + MAXC * 20], succ[MAXN * 4 + MAXC * 4]; int en;
bool vis[MAXN * 4 + MAXC * 4];
Point fi[MAXN * 4 + MAXC * 8]; int fin;
int pid(const Point &p, Point *a, int an){
	Point *ptr = std::lower_bound(a, a + an, p);
	if(ptr != a + an && *ptr == p) return ptr - a; else return -1;
}

int low[MAXN * 4 + MAXC * 8], dep[MAXN * 4 + MAXC * 8]; bool cut[MAXN * 4 + MAXC * 8];

void dfs_cut(int x, int fa){
	static int root = -1;
	if(x == fa) root = x;
	low[x] = dep[x] = dep[fa] + 1;
	int CN = 0;
	for(int c = head[x]; c != -1; c = next[c]){
		int t = til[c];
		if(t == fa) continue;
		if(dep[t]){
			if(dep[t] < low[x]) low[x] = dep[t];
		}else{
			dfs_cut(t, x);
			++CN;
			if(low[t] < low[x]) low[x] = low[t];
		}
	}
	if(x == root) cut[x] = (CN > 1); else{
		for(int c = head[x]; c != -1; c = next[c]){
			int t = til[c];
			if(dep[t] == dep[x] + 1 && low[t] >= dep[x]){
				cut[x] = 1; break;
			}
		}
	}
}

int main(){
	freopen("grid.in", "r", stdin);
	freopen("grid.out", "w", stdout);
	int T; scanf("%d", &T);
	while(T--){
		/* input */{
		int c;
		scanf("%d%d%d", &n, &m, &c);
		obn = 0;
		f(i, 0, n + 1){
			ob[obn++] = (Point) {i, 0};
			ob[obn++] = (Point) {i, m + 1};
		}
		f(j, 1, m){
			ob[obn++] = (Point) {0, j};
			ob[obn++] = (Point) {n + 1, j};
		}
		while(c--){
			scanf("%d%d", &ob[obn].x, &ob[obn].y);
			++obn;
		}
		std::sort(ob, ob + obn);
		}
		/* edges */{
		xin = 0; sen = 0;
		g(i, 0, obn) g(dir, 0, 4){
			Point tar = ob[i] + D[dir];
			if(valid(tar) && pid(tar, ob, obn) == -1){
				xi[xin++] = si[sen] = ob[i] + S[dir];
				xi[xin++] = ei[sen] = ob[i] + E[dir];
				++sen;
			}
		}
		std::sort(xi, xi + xin);
		xin = std::unique(xi, xi + xin) - xi;
		en = 0;
		memset(head, 0xff, sizeof(head[0]) * xin);
		g(i, 0, sen){
			int s = pid(si[i], xi, xin), t = pid(ei[i], xi, xin);
			next[en] = head[s]; from[en] = s; til[en] = t;
			head[s] = en++;
		}
		}
		/* successors */{
		g(i, 0, en){
			int t = til[i];
			int dt = 0;
			for(int c = head[t]; c != -1; c = next[c]) ++dt;
			if(dt == 1) succ[i] = head[t];
			else if(dt == 2){
				int tt = pid(xi[t] + leftTurn(xi[t] - xi[from[i]]), xi, xin);
				for(int c = head[t]; c != -1; c = next[c]) if(til[c] == tt){
					succ[i] = c; goto found;
				}
				printf("edge not found %d\n", i); exit(1);
				found:;
			}else{
				printf("degree error\n"); exit(1);
			}
		}
		}
		/* find loops and final points */{
		fin = 0;
		memset(vis, 0, sizeof(vis[0]) * en);
		int nii = -1;
		g(i, 0, en) if(!vis[i]){
			LL S2 = 0;
			Point ld = xi[til[i]] - xi[from[i]];
			for(int c = succ[i]; !vis[c]; c = succ[c]){
				vis[c] = 1;
				Point &p1 = xi[from[c]];
				Point &p2 = xi[til[c]];
				Point d = p2 - p1;
				Point up = p1 + (Point) {d.x == 1 || d.y == -1, d.x == 1 || d.y == 1};
				fi[fin++] = up;
				if(rightTurn(ld) == d)
					fi[fin++] = up - d;
				S2 += (LL) p1.x * p2.y - (LL) p1.y * p2.x;
				ld = d;
			}
			if(S2 > 0){
				if(nii == -1) nii = i; else{
					// f(i, 1, n){
						// f(j, 1, m) if(pid((Point) {i, j}, ob, obn) != -1) putchar('X');
						// else putchar('.');
						// putchar('\n');
					// }
					goto notconnected;
				}
			}
		}
		if(nii == -1){
			// f(i, 1, n){
				// f(j, 1, m) if(pid((Point) {i, j}, ob, obn) != -1) putchar('X');
				// else putchar('.');
				// putchar('\n');
			// }
			goto nosolution;
		}
		std::sort(fi, fi + fin);
		fin = std::unique(fi, fi + fin) - fi;
		}
		// f(i, 1, n){
			// f(j, 1, m) if(pid((Point) {i, j}, ob, obn) != -1) putchar('X');
			// else if(pid((Point) {i, j}, fi, fin) != -1) putchar('F');
			// else putchar('.');
			// putchar('\n');
		// }
		// g(i, 0, fin) printf("fin %d %d\n", fi[i].x, fi[i].y);
		/* build final graph */{
		en = 0;
		memset(head, 0xff, sizeof(head[0]) * fin);
		g(i, 0, fin) g(dir, 0, 2){
			Point tar = fi[i] + D[dir];
			int j = pid(tar, fi, fin);
			if(valid(tar) && j != -1){
				til[en] = j; next[en] = head[i];
				head[i] = en++;
				til[en] = i; next[en] = head[j];
				head[j] = en++;
			}
		}
		}
		/* find cut and get final result */{
		memset(low, 0, sizeof(low[0]) * fin);
		memset(dep, 0, sizeof(dep[0]) * fin);
		memset(cut, 0, sizeof(cut[0]) * fin);
		g(i, 0, fin) if(!dep[i]) dfs_cut(i, i);
		g(i, 0, fin) if(cut[i]){
			// printf("cut %d %d ", fi[i].x, fi[i].y);
			goto havecut;
		}
		if(fin <= 2) goto nosolution;
		goto strongconnected;
		}
		notconnected:
			printf("0\n");
			continue;
		nosolution:
			printf("-1\n");
			continue;
		havecut:
			printf("1\n");
			continue;
		strongconnected:
			printf("2\n");
			continue;
	}
	return 0;
}
