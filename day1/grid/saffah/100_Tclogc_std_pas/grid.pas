program grid;

var fin: text;
var fout: text;

type longintPointer = ^longint;
function pos(start: longintPointer; finish: longintPointer; t: longint): longint;
	var l: longint;
	var r: longint;
	var mid: longint;
begin
	if start = finish then exit(-1000000000);
	l := 0; r := finish - start - 1;
	while l < r do begin
		mid := (l + r) shr 1;
		if start[mid] < t then l := mid + 1 else r := mid;
	end;
	if start[l] <> t then pos := -1000000000 else pos := l;
end;
procedure sort(start: longintPointer; finish: longintPointer);
	var i: longintPointer;
	var j: longintPointer;
	var x: longint;
	var y: longint;
begin
	if start >= finish then exit;
	i := start; j := finish - 1;
	x := start[random(finish - start)];
	repeat
		while i^ < x do inc(i);
		while x < j^ do dec(j);
		if i <= j then begin
			y := i^; i^ := j^; j^ := y;
			inc(i); dec(j);
		end;
	until i > j;
	if start < j then sort(start, j + 1);
	if i < finish then sort(i, finish);
end;
function unique(start: longintPointer; finish: longintPointer): longintPointer;
	var y: longint;
begin
	if start = finish then exit(finish);
	unique := start;
	while true do begin
		inc(start);
		if start = finish then break;
		if unique^ = start^ then continue;
		inc(unique);
		if unique <> start then begin
			y := unique^; unique^ := start^; start^ := y;
		end;
	end;
	inc(unique);
end;

const maxC = 102400;
const fastWidth = 5;

type Point = record
	x: longint;
	y: longint;
end;
type PointPointer = ^Point;
type PointPointerPointer = ^PointPointer;

operator <(a: Point; b: Point) ret: boolean;
begin
	ret := (a.x < b.x) or ((a.x = b.x) and (a.y < b.y));
end;
operator =(a: Point; b: Point) ret: boolean;
begin
	ret := (a.x = b.x) and (a.y = b.y);
end;
operator <>(a: Point; b: Point) ret: boolean;
begin
	ret := (a.x <> b.x) or (a.y <> b.y);
end;
operator +(a: Point; b: Point) ret: Point;
begin
	ret.x := a.x + b.x; ret.y := a.y + b.y;
end;
function pos(start: PointPointer; finish: PointPointer; t: Point): longint;
var l: longint;
var r: longint;
var mid: longint;
begin
	if start = finish then exit(-1000000000);
	l := 0; r := finish - start - 1;
	while l < r do begin
		mid := (l + r) shr 1;
		if start[mid] < t then l := mid + 1 else r := mid;
	end;
	if start[l] <> t then pos := -1000000000 else pos := l;
end;
procedure sort(start: PointPointer; finish: PointPointer);
	var i: PointPointer;
	var j: PointPointer;
	var x: Point;
	var y: Point;
begin
	if start >= finish then exit;
	i := start; j := finish - 1;
	x := start[random(finish - start)];
	repeat
		while i^ < x do inc(i);
		while x < j^ do dec(j);
		if i <= j then begin
			y := i^; i^ := j^; j^ := y;
			inc(i); dec(j);
		end;
	until i > j;
	if start < j then sort(start, j + 1);
	if i < finish then sort(i, finish);
end;
function unique(start: PointPointer; finish: PointPointer): PointPointer;
	var y: Point;
begin
	if start = finish then exit(finish);
	unique := start;
	while true do begin
		inc(start);
		if start = finish then break;
		if unique^ = start^ then continue;
		inc(unique);
		if unique <> start then begin
			y := unique^; unique^ := start^; start^ := y;
		end;
	end;
	inc(unique);
end;

var rawN: longint;
var rawM: longint;
var rawObs: array[0 .. (maxC - 1)] of Point;
var rawObsEnd: PointPointer;
procedure input();
	var obsN, i: longint;
begin
	read(fin, rawN, rawM, obsN);
	rawObsEnd := rawObs;
	for i := 1 to obsN do begin
		read(fin, rawObsEnd^.x, rawObsEnd^.y);
		inc(rawObsEnd);
	end;
end;

var n: longint;
var m: longint;
var obs: array[0 .. (maxC * 13 - 1)] of Point;
var obsEnd: PointPointer;
var disX: array[0 .. (maxC * 5 - 1)] of longint;
var disXEnd: longintPointer;
var disY: array[0 .. (maxC * 5 - 1)] of longint;
var disYEnd: longintPointer;
var sortTmp: array[0 .. (maxC * 13 - 1)] of Point;
var sortCnt: array[0 .. (maxC * 5 - 1)] of longint;
procedure fastSort(start: PointPointer; finish: PointPointer);
	var curPoint: PointPointer;
	var sortTmpEnd: PointPointer;
	var i: longint;
begin
	if start = finish then exit;
	fillchar(sortCnt, sizeof(sortCnt[0]) * (m + 2), 0);
	curPoint := start;
	while curPoint <> finish do begin
		inc(sortCnt[curPoint^.y]);
		inc(curPoint);
	end;
	for i := 0 to m do inc(sortCnt[i + 1], sortCnt[i]);
	curPoint := finish - 1;
	while true do begin
		dec(sortCnt[curPoint^.y]);
		sortTmp[sortCnt[curPoint^.y]] := curPoint^;
		if curPoint = start then break;
		dec(curPoint);
	end;
	sortTmpEnd := sortTmp;
	inc(sortTmpEnd, finish - start);
	fillchar(sortCnt, sizeof(sortCnt[0]) * (n + 2), 0);
	curPoint := sortTmp;
	while curPoint <> sortTmpEnd do begin
		inc(sortCnt[curPoint^.x]);
		inc(curPoint);
	end;
	for i := 0 to n do inc(sortCnt[i + 1], sortCnt[i]);
	curPoint := sortTmpEnd - 1;
	while true do begin
		dec(sortCnt[curPoint^.x]);
		start[sortCnt[curPoint^.x]] := curPoint^;
		if curPoint = PointPointer(sortTmp) then break;
		dec(curPoint);
	end;
end;

procedure discrete(naive: boolean);
	var curPoint: PointPointer;
	var i: longint;
	var j: longint;
begin
	disXend := disX; disYEnd := disY;
	disXEnd^ := 0; inc(disXEnd);
	disXEnd^ := 1; inc(disXEnd);
	disXEnd^ := 2; inc(disXEnd);
	disXEnd^ := rawN - 1; inc(disXEnd);
	disXEnd^ := rawN; inc(disXEnd);
	disXEnd^ := rawN + 1; inc(disXEnd);
	disYEnd^ := 0; inc(disYEnd);
	disYEnd^ := 1; inc(disYEnd);
	disYEnd^ := 2; inc(disYEnd);
	disYEnd^ := rawM - 1; inc(disYEnd);
	disYEnd^ := rawM; inc(disYEnd);
	disYEnd^ := rawM + 1; inc(disYEnd);
	curPoint := rawObs;
	while curPoint <> rawObsEnd do begin
		if naive then begin
			if curPoint^.x > 1 then begin
				disXEnd^ := curPoint^.x - 2; inc(disXEnd);
			end;
			if curPoint^.y > 1 then begin
				disYEnd^ := curPoint^.y - 2; inc(disYEnd);
			end;
		end;
		disXEnd^ := curPoint^.x - 1; inc(disXEnd);
		disXEnd^ := curPoint^.x; inc(disXEnd);
		disXEnd^ := curPoint^.x + 1; inc(disXEnd);
		disYEnd^ := curPoint^.y - 1; inc(disYEnd);
		disYEnd^ := curPoint^.y; inc(disYEnd);
		disYEnd^ := curPoint^.y + 1; inc(disYEnd);
		if naive then begin
			if curPoint^.x < rawN then begin
				disXEnd^ := curPoint^.x + 2; inc(disXEnd);
			end;
			if curPoint^.y < rawM then begin
				disYEnd^ := curPoint^.y + 2; inc(disYEnd);
			end;
		end;
		inc(curPoint);
	end;
	sort(disX, disXEnd); disXEnd := unique(disX, disXEnd);
	sort(disY, disYEnd); disYEnd := unique(disY, disYEnd);
	n := disXEnd - longintPointer(disX) - 2; m := disYEnd - longintPointer(disY) - 2;
	obsEnd := obs;
	curPoint := rawObs;
	while curPoint <> rawObsEnd do begin
		obsEnd^.x := pos(disX, disXEnd, curPoint^.x);
		obsEnd^.y := pos(disY, disYEnd, curPoint^.y);
		inc(obsEnd);
		inc(curPoint);
	end;
	for j := 0 to m + 1 do begin
		obsEnd^.x := 0; obsEnd^.y := j; inc(obsEnd);
		obsEnd^.x := n + 1; obsEnd^.y := j; inc(obsEnd);
	end;
	for i := 1 to n do begin
		obsEnd^.x := i; obsEnd^.y := 0; inc(obsEnd);
		obsEnd^.x := i; obsEnd^.y := m + 1; inc(obsEnd);
	end;
	fastSort(obs, obsEnd); obsEnd := unique(obs, obsEnd);
end;

var obsNL: array[0 .. (fastWidth - 1), 0 .. (maxC * 5 - 1)] of longint;
var obsNH: array[0 .. (fastWidth - 1), 0 .. (maxC * 5 - 1)] of longint;
var obsML: array[0 .. (fastWidth - 1), 0 .. (maxC * 5 - 1)] of longint;
var obsMH: array[0 .. (fastWidth - 1), 0 .. (maxC * 5 - 1)] of longint;
var obsIL: array[0 .. (maxC * 5 - 1)] of PointPointer;
var obsIR: array[0 .. (maxC * 5 - 1)] of PointPointer;
function fastObsPos(curPoint: Point): longint;
begin
	if curPoint.x < fastWidth then fastObsPos := obsNL[curPoint.x][curPoint.y]
	else if curPoint.x > n + 1 - fastWidth then fastObsPos := obsNH[n + 1 - curPoint.x][curPoint.y]
	else if curPoint.y < fastWidth then fastObsPos := obsML[curPoint.y][curPoint.x]
	else if curPoint.y > m + 1 - fastWidth then fastObsPos := obsMH[m + 1 - curPoint.y][curPoint.x]
	else fastObsPos := pos(obsIL[curPoint.x], obsIR[curPoint.x], curPoint) + (obsIL[curPoint.x] - PointPointer(obs));
end;
procedure fastObsInit();
var outL: PointPointer;
var outR: PointPointer;
var cx: longint;
var i: longint;
var id: longint;
var curObs: PointPointer;
begin
	outL := obs;
	while outL < obsEnd do begin
		cx := outL^.x;
		outR := outL;
		while (outR + 1 < obsEnd) and (outR[1].x = cx) do inc(outR);
		obsIL[cx] := outL; obsIR[cx] := outR;
		while (obsIL[cx] <= outR) and (obsIL[cx]^.y < fastWidth) do inc(obsIL[cx]);
		while (obsIR[cx] >= outL) and (obsIR[cx]^.y > m + 1 - fastWidth) do dec(obsIR[cx]);
		inc(obsIR[cx]);
		outL := outR + 1;
	end;
	for i := 0 to fastWidth - 1 do begin
		fillchar(obsNL[i], sizeof(obsNL[i][0]) * (m + 2), $ff);
		fillchar(obsNH[i], sizeof(obsNH[i][0]) * (m + 2), $ff);
		fillchar(obsML[i], sizeof(obsML[i][0]) * (n + 2), $ff);
		fillchar(obsMH[i], sizeof(obsMH[i][0]) * (n + 2), $ff);
	end;
	id := 0;
	curObs := obs;
	while curObs <> obsEnd do begin
		if curObs^.x < fastWidth then obsNL[curObs^.x][curObs^.y] := id
		else if curObs^.x > n + 1 - fastWidth then obsNH[n + 1 - curObs^.x][curObs^.y] := id
		else if curObs^.y < fastWidth then obsML[curObs^.y][curObs^.x] := id
		else if curObs^.y > m + 1 - fastWidth then obsMH[m + 1 - curObs^.y][curObs^.x] := id;
		inc(curObs); inc(id);
	end;
end;

var obsList0: array[0 .. (maxC * 13 - 1)] of Point;
var obsList0End: PointPointer;
var obsList0Begin: array[0 .. maxC] of PointPointer;
var obsList0BeginEnd: PointPointerPointer;

function isValid(curPoint: Point): boolean;
begin
	isValid := (curPoint.x >= 0) and (curPoint.x <= n + 1) and (curPoint.y >= 0) and (curPoint.y <= m + 1);
end;

var obsVisited: array[0 .. (maxC * 13 - 1)] of boolean;
const D8: array[0 .. 7] of Point = ((x: -1; y: -1), (x: -1; y: 0), (x: -1; y: 1), (x: 0; y: -1), (x: 0; y: 1), (x: 1; y: -1), (x: 1; y: 0), (x: 1; y: 1));
procedure dfsBranches(curPoint: Point; obsId: longint);
var tar: Point;
var tarId: longint;
var dir: longint;
begin
	if obsVisited[obsId] then exit;
	obsVisited[obsId] := true; obsList0End^ := curPoint; inc(obsList0End);
	for dir := 0 to 7 do begin
		tar := curPoint + D8[dir];
		if isValid(tar) then begin
			tarId := fastObsPos(tar);
			if tarId >= 0 then dfsBranches(tar, tarId);
		end;
	end;
end;
procedure branches();
	var obsN: longint;
	var i :longint;
begin
	obsN := obsEnd - PointPointer(obs);
	fillchar(obsVisited, sizeof(obsVisited[0]) * obsN, 0);
	obsList0End := obsList0;
	obsList0BeginEnd := obsList0Begin;
	for i := 0 to obsN - 1 do if not obsVisited[i] then begin
		obsList0BeginEnd^ := obsList0End;
		dfsBranches(obs[i], i);
		inc(obsList0BeginEnd);
	end;
	obsList0BeginEnd^ := obsList0End;
end;

type PointT = record
	x: longint;
	y: longint;
	t: shortint;
end;
type PointTPointer = ^PointT;
type PointTPointerPointer = ^PointTPointer;

operator <(a: PointT; b: PointT) ret: boolean;
begin
	ret := (a.x < b.x) or ((a.x = b.x) and (a.y < b.y)) or ((a.x = b.x) and (a.y = b.y) and (a.t < b.t));
end;
operator =(a: PointT; b: PointT) ret: boolean;
begin
	ret := (a.x = b.x) and (a.y = b.y);
end;
operator <>(a: PointT; b: PointT) ret: boolean;
begin
	ret := (a.x <> b.x) or (a.y <> b.y);
end;
function pos(start: PointTPointer; finish: PointTPointer; t: PointT): longint;
var l: longint;
var r: longint;
var mid: longint;
begin
	if start = finish then exit(-1000000000);
	l := 0; r := finish - start - 1;
	while l < r do begin
		mid := (l + r) shr 1;
		if start[mid] < t then l := mid + 1 else r := mid;
	end;
	if start[l] <> t then pos := -1000000000 else pos := l;
end;
procedure sort(start: PointTPointer; finish: PointTPointer);
	var i: PointTPointer;
	var j: PointTPointer;
	var x: PointT;
	var y: PointT;
begin
	if start >= finish then exit;
	i := start; j := finish - 1;
	x := start[random(finish - start)];
	repeat
		while i^ < x do inc(i);
		while x < j^ do dec(j);
		if i <= j then begin
			y := i^; i^ := j^; j^ := y;
			inc(i); dec(j);
		end;
	until i > j;
	if start < j then sort(start, j + 1);
	if i < finish then sort(i, finish);
end;
function unique(start: PointTPointer; finish: PointTPointer): PointTPointer;
	var y: PointT;
begin
	if start = finish then exit(finish);
	unique := start;
	while true do begin
		inc(start);
		if start = finish then break;
		if unique^ = start^ then continue;
		inc(unique);
		if unique <> start then begin
			y := unique^; unique^ := start^; start^ := y;
		end;
	end;
	inc(unique);
end;
var sortFleaTmp: array[0 .. (maxC * 48 - 1)] of PointT;
procedure fastSort(start: PointTPointer; finish: PointTPointer);
var l: PointTPointer;
var r: PointTPointer;
var y: PointT;
var curPoint: PointTPointer;
var i: longint;
var sortTmpEnd: PointTPointer;
begin
	if finish - start < n + m then begin
		sort(start, finish); exit;
	end;
	l := start; r := finish - 1;
	while true do begin
		while (l < finish) and (l^.t = 1) do inc(l);
		while (r >= start) and (r^.t = 2) do dec(r);
		if l >= r then break;
		y := l^; l^ := r^; r^ := y;
	end;
	fillchar(sortCnt, sizeof(sortCnt[0]) * (m + 2), 0);
	curPoint := start;
	while curPoint <> finish do begin
		inc(sortCnt[curPoint^.y]); inc(curPoint);
	end;
	for i := 0 to m do inc(sortCnt[i + 1], sortCnt[i]);
	curPoint := finish - 1;
	while true do begin
		dec(sortCnt[curPoint^.y]);
		sortFleaTmp[sortCnt[curPoint^.y]] := curPoint^;
		if curPoint = start then break;
		dec(curPoint);
	end;
	sortTmpEnd := PointTPointer(sortFleaTmp) + (finish - start);
	fillchar(sortCnt, sizeof(sortCnt[0]) * (n + 2), 0);
	curPoint := sortFleaTmp;
	while curPoint <> sortTmpEnd do begin
		inc(sortCnt[curPoint^.x]); inc(curPoint);
	end;
	for i := 0 to n do inc(sortCnt[i + 1], sortCnt[i]);
	curPoint := sortTmpEnd - 1;
	while true do begin
		dec(sortCnt[curPoint^.x]);
		start[sortCnt[curPoint^.x]] := curPoint^;
		if curPoint = PointTPointer(sortFleaTmp) then break;
		dec(curPoint);
	end;
end;

var fleaList12: array[0 .. (maxC * 48 - 1)] of PointT;
var fleaList12End: PointTPointer;
var fleaList12Begin: array[0 .. (maxC - 1)] of PointTPointer;
var fleaList12BeginEnd: PointTPointerPointer;
const D16: array[0 .. 15] of Point = ((x: -2; y: -2), (x: -2; y: -1), (x: -2; y: 0), (x: -2; y: 1), (x: -2; y: 2), (x: -1; y: -2), (x: -1; y: 2),
(x: 0; y: -2), (x: 0; y: 2), (x: 1; y: -2), (x: 1; y: 2), (x: 2; y: -2), (x: 2; y: -1), (x: 2; y: 0), (x: 2; y: 1), (x: 2; y: 2));
procedure getFleasByBranches();
	var curObsList0Begin: PointPointerPointer;
	var curObs: PointPointer;
	var i: longint;
	var dir: longint;
	var tar: Point;
begin
	fleaList12End := fleaList12;
	fleaList12BeginEnd := fleaList12Begin;
	curObsList0Begin := obsList0Begin;
	while curObsList0Begin <> obsList0BeginEnd do begin
		fleaList12BeginEnd^ := fleaList12End;
		curObs := curObsList0Begin^;
		while curObs <> curObsList0Begin[1] do begin
			if curObs^.x = 0 then for i := 0 to 1 do begin
				tar := curObs^; inc(tar.x, i);
				if fastObsPos(tar) < 0 then begin
					fleaList12End^.x := tar.x;
					fleaList12End^.y := tar.y;
					fleaList12End^.t := 1;
					inc(fleaList12End);
				end;
			end else if curObs^.x > n then for i := 0 to 1 do begin
				tar := curObs^; dec(tar.x, i);
				if fastObsPos(tar) < 0 then begin
					fleaList12End^.x := tar.x;
					fleaList12End^.y := tar.y;
					fleaList12End^.t := 1;
					inc(fleaList12End);
				end;
			end else if curObs^.y = 0 then for i := 0 to 1 do begin
				tar := curObs^; inc(tar.y, i);
				if fastObsPos(tar) < 0 then begin
					fleaList12End^.x := tar.x;
					fleaList12End^.y := tar.y;
					fleaList12End^.t := 1;
					inc(fleaList12End);
				end;
			end else if curObs^.y > m then for i := 0 to 1 do begin
				tar := curObs^; dec(tar.y, i);
				if fastObsPos(tar) < 0 then begin
					fleaList12End^.x := tar.x;
					fleaList12End^.y := tar.y;
					fleaList12End^.t := 1;
					inc(fleaList12End);
				end;
			end else begin
				for dir := 0 to 7 do begin
					tar := curObs^ + D8[dir];
					if isValid(tar) and (fastObsPos(tar) < 0) then begin
						fleaList12End^.x := tar.x;
						fleaList12End^.y := tar.y;
						fleaList12End^.t := 1;
						inc(fleaList12End);
					end;
				end;
				for dir := 0 to 15 do begin
					tar := curObs^ + D16[dir];
					if isValid(tar) and (fastObsPos(tar) < 0) then begin
						fleaList12End^.x := tar.x;
						fleaList12End^.y := tar.y;
						fleaList12End^.t := 2;
						inc(fleaList12End);
					end;
				end;
			end;
			inc(curObs);
		end;
		fastSort(fleaList12BeginEnd^, fleaList12End);
		fleaList12End := unique(fleaList12BeginEnd^, fleaList12End);
		inc(fleaList12BeginEnd); inc(curObsList0Begin);
	end;
	fleaList12BeginEnd^ := fleaList12End;
end;

var curBranchFlea12ListBegin: PointTPointer;
var curBranchFlea12ListEnd: PointTPointer;
var fleaNL: array[0 .. (fastWidth - 1), 0 .. (maxC * 5 - 1)] of longint;
var fleaNH: array[0 .. (fastWidth - 1), 0 .. (maxC * 5 - 1)] of longint;
var fleaML: array[0 .. (fastWidth - 1), 0 .. (maxC * 5 - 1)] of longint;
var fleaMH: array[0 .. (fastWidth - 1), 0 .. (maxC * 5 - 1)] of longint;
var fleaIL: array[0 .. (maxC * 5 - 1)] of PointTPointer;
var fleaIR: array[0 .. (maxC * 5 - 1)] of PointTPointer;
function specialFastPos(curPoint: PointT): longint;
begin
	if curPoint.x < fastWidth then specialFastPos := fleaNL[curPoint.x][curPoint.y]
	else if curPoint.x > n + 1 - fastWidth then specialFastPos := fleaNH[n + 1 - curPoint.x][curPoint.y]
	else if curPoint.y < fastWidth then specialFastPos := fleaML[curPoint.y][curPoint.x]
	else if curPoint.y > m + 1 - fastWidth then specialFastPos := fleaMH[m + 1 - curPoint.y][curPoint.x]
	else specialFastPos := pos(fleaIL[curPoint.x], fleaIR[curPoint.x], curPoint) + (fleaIL[curPoint.x] - curBranchFlea12ListBegin);
end;
procedure specialFastFleaInit();
	var outL: PointTPointer;
	var cx: longint;
	var outR: PointTPointer;
	var i: longint;
	var id: longint;
	var curFlea: PointTPointer;
begin
	for i := 0 to n + 1 do begin
		fleaIL[i] := curBranchFlea12ListBegin; fleaIR[i] := curBranchFlea12ListBegin;
	end;
	outL := curBranchFlea12ListBegin;
	while outL < curBranchFlea12ListEnd do begin
		cx := outL^.x;
		outR := outL;
		while (outR + 1 < curBranchFlea12ListEnd) and (outR[1].x = cx) do inc(outR);
		fleaIL[cx] := outL; fleaIR[cx] := outR;
		while (fleaIL[cx] <= outR) and (fleaIL[cx]^.y < fastWidth) do inc(fleaIL[cx]);
		while (fleaIR[cx] >= outL) and (fleaIR[cx]^.y > m + 1 - fastWidth) do dec(fleaIR[cx]);
		inc(fleaIR[cx]);
		outL := outR + 1;
	end;
	for i := 0 to fastWidth - 1 do begin
		fillchar(fleaNL[i], sizeof(fleaNL[i][0]) * (m + 2), $ff);
		fillchar(fleaNH[i], sizeof(fleaNH[i][0]) * (m + 2), $ff);
		fillchar(fleaML[i], sizeof(fleaML[i][0]) * (n + 2), $ff);
		fillchar(fleaMH[i], sizeof(fleaMH[i][0]) * (n + 2), $ff);
	end;
	id := 0;
	curFlea := curBranchFlea12ListBegin;
	while curFlea <> curBranchFlea12ListEnd do begin
		if curFlea^.x < fastWidth then fleaNL[curFlea^.x][curFlea^.y] := id
		else if curFlea^.x > n + 1 - fastWidth then fleaNH[n + 1 - curFlea^.x][curFlea^.y] := id
		else if curFlea^.y < fastWidth then fleaML[curFlea^.y][curFlea^.x] := id
		else if curFlea^.y > m + 1 - fastWidth then fleaMH[m + 1 - curFlea^.y][curFlea^.x] := id;
		inc(curFlea); inc(id);
	end;
end;
var fleaVisited: array[0 .. (maxC * 48 - 1)] of boolean;
var enableFastFlea: boolean;
const D4: array[0 .. 3] of Point = ((x: -1; y: 0), (x: 0; y: -1), (x: 0; y: 1), (x: 1; y: 0));
procedure dfsFleaBranches(curPoint: PointT; fleaId: longint);
	var tar: PointT;
	var tarId: longint;
	var dir: longint;
begin
	if fleaVisited[fleaId] then exit;
	fleaVisited[fleaId] := true;
	for dir := 0 to 3 do begin
		tar.x := curPoint.x + D4[dir].x; tar.y := curPoint.y + D4[dir].y;
		tar.t := 0;
		if enableFastFlea then tarId := specialFastPos(tar)
		else tarId := pos(curBranchFlea12ListBegin, curBranchFlea12ListEnd, tar);
		if (tarId >= 0) and (curBranchFlea12ListBegin[tarId].t = 1) then dfsFleaBranches(curBranchFlea12ListBegin[tarId], tarId);
	end;
end;
function checkFleaConnectivity(): boolean;
	var curFleaList12Begin: PointTPointerPointer;
	var curFleaN: longint;
	var i: longint;
begin
	curFleaList12Begin := fleaList12Begin;
	while curFleaList12Begin <> fleaList12BeginEnd do begin
		curBranchFlea12ListBegin := curFleaList12Begin^;
		curBranchFlea12ListEnd := curFleaList12Begin[1];
		inc(curFleaList12Begin);
		curFleaN := curBranchFlea12ListEnd - curBranchFlea12ListBegin;
		if curFleaN > n + m then begin
			enableFastFlea := true;
			specialFastFleaInit();
		end else enableFastFlea := false;
		fillChar(fleaVisited, sizeof(fleaVisited[0]) * curFleaN, 0);
		for i := 0 to curFleaN - 1 do if curBranchFlea12ListBegin[i].t = 1 then begin
			dfsFleaBranches(curBranchFlea12ListBegin[i], 0);
			break;
		end;
		for i := 0 to curFleaN - 1 do if (curBranchFlea12ListBegin[i].t = 1) and not fleaVisited[i] then exit(false);
	end;
	checkFleaConnectivity := true;
end;

function fastFleaPos(curPoint: PointT): longint;
begin
	if curPoint.x < fastWidth then fastFleaPos := fleaNL[curPoint.x][curPoint.y]
	else if curPoint.x > n + 1 - fastWidth then fastFleaPos := fleaNH[n + 1 - curPoint.x][curPoint.y]
	else if curPoint.y < fastWidth then fastFleaPos := fleaML[curPoint.y][curPoint.x]
	else if curPoint.y > m + 1 - fastWidth then fastFleaPos := fleaMH[m + 1 - curPoint.y][curPoint.x]
	else fastFleaPos := pos(fleaIL[curPoint.x], fleaIR[curPoint.x], curPoint) + (fleaIL[curPoint.x] - PointTPointer(fleaList12));
end;
procedure fastFleaInit();
	var i: longint;
	var outL: PointTPointer;
	var cx: longint;
	var outR: PointTPointer;
	var id: longint;
	var curFlea: PointTPointer;
begin
	fastSort(fleaList12, fleaList12End);
	fleaList12End := unique(fleaList12, fleaList12End);
	for i := 0 to n + 1 do begin
		fleaIL[i] := fleaList12; fleaIR[i] := fleaList12;
	end;
	outL := fleaList12;
	while outL < fleaList12End do begin
		cx := outL^.x;
		outR := outL;
		while (outR + 1 < fleaList12End) and (outR[1].x = cx) do inc(outR);
		fleaIL[cx] := outL; fleaIR[cx] := outR;
		while (fleaIL[cx] <= outR) and (fleaIL[cx]^.y < fastWidth) do inc(fleaIL[cx]);
		while (fleaIR[cx] >= outL) and (fleaIR[cx]^.y > m + 1 - fastWidth) do dec(fleaIR[cx]);
		inc(fleaIR[cx]);
		outL := outR + 1;
	end;
	for i := 0 to fastWidth - 1 do begin
		fillchar(fleaNL[i], sizeof(fleaNL[i][0]) * (m + 2), $ff);
		fillchar(fleaNH[i], sizeof(fleaNH[i][0]) * (m + 2), $ff);
		fillchar(fleaML[i], sizeof(fleaML[i][0]) * (n + 2), $ff);
		fillchar(fleaMH[i], sizeof(fleaMH[i][0]) * (n + 2), $ff);
	end;
	id := 0;
	curFlea := fleaList12;
	while curFlea <> fleaList12End do begin
		if curFlea^.x < fastWidth then fleaNL[curFlea^.x][curFlea^.y] := id
		else if curFlea^.x > n + 1 - fastWidth then fleaNH[n + 1 - curFlea^.x][curFlea^.y] := id
		else if curFlea^.y < fastWidth then fleaML[curFlea^.y][curFlea^.x] := id
		else if curFlea^.y > m + 1 - fastWidth then fleaMH[m + 1 - curFlea^.y][curFlea^.x] := id;
		inc(curFlea); inc(id);
	end;
end;

type AdjNode = record
	til: longint;
	next: ^AdjNode;
end;
var head: array[0 .. (maxC * 48 - 1)] of ^AdjNode;
var adjList: array[0 .. (maxC * 192 - 1)] of AdjNode;
var adjListEnd: ^AdjNode;
procedure buildFinalGraph();
	var fleaN: longint;
	var p: longint;
	var dir: longint;
	var tar: PointT;
	var op: longint;
begin
	fleaN := fleaList12End - PointTPointer(fleaList12);
	adjListEnd := adjList;
	fillchar(head, sizeof(head[0]) * fleaN, 0);
	for p := 0 to fleaN - 1 do for dir := 0 to 1 do begin
		tar.x := fleaList12[p].x + 1 - dir;
		tar.y := fleaList12[p].y + dir;
		tar.t := 0;
		op := fastFleaPos(tar);
		if op < 0 then continue;
		adjListEnd^.til := op; adjListEnd^.next := head[p];
		head[p] := adjListEnd; inc(adjListEnd);
		adjListEnd^.til := p; adjListEnd^.next := head[op];
		head[op] := adjListEnd; inc(adjListEnd);
	end;
end;

var low: array[0 .. (maxC * 48 - 1)] of longint;
var dep: array[0 .. (maxC * 48 - 1)] of longint;
var cut: array[0 .. (maxC * 48 - 1)] of boolean;
var dfsRoot: longint;
procedure dfsCut(x: longint; fa: longint);
	var CN: longint;
	var c: ^AdjNode;
	var t: longint;
begin
	// writeln('dfsCut ', x, ' ', fa);
	if x = fa then dfsRoot := x;
	dep[x] := dep[fa] + 1; low[x] := dep[x];
	CN := 0;
	c := head[x];
	while c <> nil do begin
		t := c^.til;
		if t = fa then begin
			c := c^.next; continue;
		end;
		if dep[t] <> 0 then begin
			if dep[t] < low[x] then low[x] := dep[t];
		end else begin
			dfsCut(t, x);
			inc(CN);
			if low[t] < low[x] then low[x] := low[t];
		end;
		c := c^.next;
	end;
	if x = dfsRoot then cut[x] := (CN > 1) else begin
		c := head[x];
		while c <> nil do begin
			t := c^.til;
			if (dep[t] = dep[x] + 1) and (low[t] >= dep[x]) then begin
				cut[x] := true; break;
			end;
			c := c^.next;
		end;
	end;
end;
function checkCut(): boolean;
	var fleaN: longint;
	var i: longint;
begin
	fleaN := fleaList12End - PointTPointer(fleaList12);
	fillchar(low, sizeof(low[0]) * fleaN, 0);
	fillchar(dep, sizeof(dep[0]) * fleaN, 0);
	fillchar(cut, sizeof(cut[0]) * fleaN, 0);
	for i := 0 to fleaN - 1 do if dep[i] = 0 then dfsCut(i, i);
	for i := 0 to fleaN - 1 do if cut[i] and (fleaList12[i].t = 1) then exit(true);
	checkCut := false;
end;

procedure main();
	var i: longint;
	var T: longint;
begin
	read(fin, T);
	for i := 1 to T do begin
		input();
		discrete((rawN = 1) or (rawM = 1));
		fastObsInit();
		branches();
		getFleasByBranches();
		if fleaList12End = PointTPointer(fleaList12) then begin
			writeln(fout, -1); continue;
		end;
		if not checkFleaConnectivity() then begin
			writeln(fout, 0); continue;
		end;
		fastFleaInit();
		buildFinalGraph();
		if checkCut() then begin
			writeln(fout, 1); continue;
		end;
		if fleaList12End - PointTPointer(fleaList12) <= 2 then writeln(fout, -1) else writeln(fout, 2);
	end;
end;

begin
	assign(fin, 'grid.in'); reset(fin);
	assign(fout, 'grid.out'); rewrite(fout);
	randomize();
	main();
	close(fin); close(fout);
end.
