#include <cstdio>
#include <cstring>
#include <algorithm>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
#define g(x, y, z) for(int x = (y); x < (z); ++x)
#define h(x, y, z) for(int x = (y); x >= (z); --x)

int n, m, c;
int dx[500086], dy[500086]; int dn, dm;
int ox[100086], oy[100086];
bool av[2000086];
int low[2000086], dep[2000086]; bool cut[2000086];
int DX[4] = {-1, 1, 0, 0}, DY[4] = {0, 0, -1, 1};

inline void dfs_cut(int i, int fa){
	low[i] = dep[i] = dep[fa] + 1;
	int CN = 0, x = i / m, y = i % m;
	if(y == 0){
		--x; y = m;
	}
	g(d, 0, 4){
		int tx = x + DX[d], ty = y + DY[d];
		if(tx <= 0 || tx > n || ty <= 0 || ty > m) continue;
		int t = tx * m + ty;
		if(t == fa || !av[t]) continue;
		if(dep[t]){
			if(dep[t] < low[i]) low[i] = dep[t];
		}else{
			dfs_cut(t, i);
			++CN;
			if(low[t] < low[i]) low[i] = low[t];
		}
	}
	if(i == fa) cut[i] = (CN > 1); else g(d, 0, 4){
		int tx = x + DX[d], ty = y + DY[d];
		if(tx <= 0 || tx > n || ty <= 0 || ty > m) continue;
		int t = tx * m + ty;
		if(!av[t]) continue;
		if(dep[t] == dep[i] + 1 && low[t] >= dep[i]){
			cut[i] = 1; break;
		}
	}
}

int main(){
	freopen("grid.in", "r", stdin);
	freopen("grid.out", "w", stdout);
	int T; scanf("%d", &T);
	while(T--){
		scanf("%d%d%d", &n, &m, &c);
		memset(av, 1, sizeof(av));
		g(i, 0, c) scanf("%d%d", ox + i, oy + i);
		dn = dm = 0;
		dx[dn++] = 0;     dy[dm++] = 0;
		dx[dn++] = 1;     dy[dm++] = 1;
		dx[dn++] = 2;     dy[dm++] = 2;
		dx[dn++] = n - 1; dy[dm++] = m - 1;
		dx[dn++] = n;     dy[dm++] = m;
		dx[dn++] = n + 1; dy[dm++] = m + 1;
		g(i, 0, c){
			if(n == 1 || m == 1){
				if(ox[i] > 1) dx[dn++] = ox[i] - 2;
				if(ox[i] < n) dx[dn++] = ox[i] + 2;
				if(oy[i] > 1) dy[dm++] = oy[i] - 2;
				if(oy[i] < m) dy[dm++] = oy[i] + 2;
			}
			dx[dn++] = ox[i] - 1; dy[dm++] = oy[i] - 1;
			dx[dn++] = ox[i];     dy[dm++] = oy[i];
			dx[dn++] = ox[i] + 1; dy[dm++] = oy[i] + 1;
		}
		std::sort(dx, dx + dn); n = std::unique(dx, dx + dn) - dx - 2;
		std::sort(dy, dy + dm); m = std::unique(dy, dy + dm) - dy - 2;
		g(i, 0, c) av[(std::lower_bound(dx, dx + n + 2, ox[i]) - dx) * m + (std::lower_bound(dy, dy + m + 2, oy[i]) - dy)] = 0;
		memset(low, 0, sizeof(low));
		memset(dep, 0, sizeof(dep));
		memset(cut, 0, sizeof(cut));
		f(i, 1, n) f(j, 1, m) if(av[i * m + j]){
			dfs_cut(i * m + j, i * m + j); goto out;
		}
		out:;
		f(i, 1, n) f(j, 1, m) if(av[i * m + j] && !dep[i * m + j]) goto ans0;
		f(i, 1, n) f(j, 1, m) if(av[i * m + j] && cut[i * m + j]) goto ans1;
		if(n * m - c <= 2) goto ans_1;
		goto ans2;
		ans0:  printf("0\n");  continue;
		ans1:  printf("1\n");  continue;
		ans2:  printf("2\n");  continue;
		ans_1: printf("-1\n"); continue;
	}
	return 0;
}
