#include "testlib.h"
#include <algorithm>
#include <stdio.h>
#include <map>

const int MIN_T = 1;
const int MAX_T = 20;

const int MIN_N = 1;
const int MAX_N = 1000000000;

const int MAX_C_SUM = 100000;

int main(int argc, char **argv) {
	
	if (argc - 1 != 1) {
		fprintf(stderr, "usage: %s <test_case_id>\n", argv[0]);
		return 1;
	}
	
	registerValidation();
	
	int T = inf.readInt(MIN_T, MAX_T, "T");
	inf.readEoln();
	
	int c_sum = 0;
	
	for (int i = 1; i <= T; i++) {
		int n, m, c;
		n = inf.readInt(MIN_N, MAX_N, "n");
		inf.readSpace();
		m = inf.readInt(MIN_N, MAX_N, "m");
		inf.readSpace();
		int tmp = std::min((long long)MAX_C_SUM, 1LL * n * m);
		c = inf.readInt(0, tmp, "c");
		inf.readEoln();
		c_sum += c;
		ensuref(c_sum <= MAX_C_SUM, "sigma c must <= 1e5");
		
		std::map<long long, int> M;
		
		for (int i = 1; i <= c; i++) {
			int x, y;
			x = inf.readInt(1, n, "x");
			inf.readSpace();
			y = inf.readInt(1, m, "y");
			inf.readEoln();
			
			long long id = 1LL * (x - 1) * m + y;
			ensuref(M[id] == 0, "one gryllus must be only described once");
			M[id] = 1;
		}
	}
	
	inf.readEof();
	
	return 0;
}
